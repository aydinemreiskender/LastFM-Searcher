package com.iskae.lastfmsearcher.ui.favorites;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.android.databinding.library.baseAdapters.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseActivity;
import com.iskae.lastfmsearcher.databinding.ActivityMasterBinding;

public class FavoritesActivity extends BaseActivity<FavoritesViewModel, ActivityMasterBinding> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMasterBinding binding = getViewDataBinding();
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        addFragmentToActivity(getSupportFragmentManager(), FavoritesFragment.newInstance(),
                R.id.contentContainer, FavoritesFragment.class.getSimpleName());
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_master;
    }

    @Override
    public FavoritesViewModel getViewModel() {
        return ViewModelProviders.of(this, viewModelFactory).get(FavoritesViewModel.class);
    }
}

package com.iskae.lastfmsearcher.ui.widget;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.RemoteViewsService;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

import static com.iskae.lastfmsearcher.ui.widget.WidgetUtils.PREF_ARTISTS;

public class WidgetService extends RemoteViewsService {

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String itemType = preferences.getString(String.valueOf(appWidgetId), null);
        if (itemType != null) {
            if (itemType.equals(PREF_ARTISTS)) {
                return new WidgetArtistsProvider(this.getApplicationContext(), intent);
            } else {
                return new WidgetTracksProvider(this.getApplicationContext(), intent);
            }
        }
        return null;
    }

}
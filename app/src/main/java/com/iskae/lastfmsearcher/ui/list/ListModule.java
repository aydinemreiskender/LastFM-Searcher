package com.iskae.lastfmsearcher.ui.list;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author Emre on 17.03.18
 */

@Module
public abstract class ListModule {

    @ContributesAndroidInjector
    abstract WorldTrendingFragment contributesWorldTrendingFragment();

    @ContributesAndroidInjector
    abstract LocationTrendingFragment contributesLocationTrendingFragment();
}

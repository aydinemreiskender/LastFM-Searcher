package com.iskae.lastfmsearcher.ui.favorites;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FavoritesModule {

    @ContributesAndroidInjector
    abstract FavoritesFragment contributesFavoritesFragment();

}

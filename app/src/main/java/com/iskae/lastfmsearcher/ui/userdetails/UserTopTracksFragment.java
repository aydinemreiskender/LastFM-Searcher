package com.iskae.lastfmsearcher.ui.userdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentUserTrendingBinding;
import com.iskae.lastfmsearcher.ui.adapters.ExpandedTracksAdapter;

import java.util.ArrayList;

/**
 * @author Emre on 18.03.18
 */

public class UserTopTracksFragment extends BaseFragment<UserDetailsViewModel, FragmentUserTrendingBinding> {

    private FragmentUserTrendingBinding binding;
    private ExpandedTracksAdapter adapter;

    public static UserTopTracksFragment newInstance() {
        return new UserTopTracksFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ExpandedTracksAdapter(getContext(), new ArrayList<>());
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.rvTrendingList.setLayoutManager(layoutManager);
        binding.rvTrendingList.setItemAnimator(new DefaultItemAnimator());
        binding.rvTrendingList.setAdapter(adapter);

        getViewModel().userTopTracks.observe(this, tracksResource -> {
            binding.setUserTrendingResource(tracksResource);
            adapter.setTracks(tracksResource.getData());
        });

        getTracker().setScreenName(UserTopTracksFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user_trending;
    }

    @Override
    public UserDetailsViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(UserDetailsViewModel.class);
    }
}

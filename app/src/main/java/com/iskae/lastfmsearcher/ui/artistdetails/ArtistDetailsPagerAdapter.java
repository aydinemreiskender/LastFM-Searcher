package com.iskae.lastfmsearcher.ui.artistdetails;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.iskae.lastfmsearcher.R;

public class ArtistDetailsPagerAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_ITEMS = 3;

    private Context context;

    public ArtistDetailsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ArtistInfoFragment.newInstance();
            case 1:
                return ArtistAlbumsFragment.newInstance();
            case 2:
                return ArtistTracksFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.artist_details_tab_info);
            case 1:
                return context.getString(R.string.artist_details_tab_albums);
            case 2:
                return context.getString(R.string.artist_details_tab_tracks);
            default:
                return null;
        }
    }
}

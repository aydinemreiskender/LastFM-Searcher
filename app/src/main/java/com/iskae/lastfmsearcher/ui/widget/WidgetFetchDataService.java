package com.iskae.lastfmsearcher.ui.widget;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.iskae.lastfmsearcher.core.data.model.Artist;
import com.iskae.lastfmsearcher.core.data.model.Track;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.schedulers.Schedulers;

import static com.iskae.lastfmsearcher.ui.widget.WidgetUtils.PREF_ARTISTS;

public class WidgetFetchDataService extends Service {
    private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private Disposable disposable = Disposables.empty();
    public static List<Artist> artistsList;
    public static List<Track> trackList;

    @Inject
    WidgetUtils widgetUtils;

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.hasExtra(AppWidgetManager.EXTRA_APPWIDGET_ID)) {
            appWidgetId = intent.getIntExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            String itemType = widgetUtils.getWidgetItemType(appWidgetId);
            if (itemType != null) {
                if (itemType.equals(PREF_ARTISTS)) {
                    disposable = widgetUtils.getWorldTopArtistsList().observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(topArtistsResponseResponse -> {
                                if (topArtistsResponseResponse.body() != null &&
                                        topArtistsResponseResponse.body().getArtistsResponse().getArtists() != null) {
                                    artistsList = topArtistsResponseResponse.body().
                                            getArtistsResponse().getArtists();
                                    populateWidget();
                                }
                            });
                } else {
                    disposable = widgetUtils.getWorldTopTracksList().observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(topTracksResponse -> {
                                if (topTracksResponse.body() != null &&
                                        topTracksResponse.body().getTracksResponse().getTracks() != null) {
                                    trackList = topTracksResponse.body().
                                            getTracksResponse().getTracks();
                                    populateWidget();
                                }
                            });
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void populateWidget() {
        Intent serviceIntent = new Intent(this, UpdateWidgetService.class);
        serviceIntent.setAction(LastFmSearcherWidgetProvider.DATA_FETCHED);
        serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        startService(serviceIntent);
    }
}

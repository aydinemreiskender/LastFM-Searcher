package com.iskae.lastfmsearcher.ui.userdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentUserInfoBinding;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

/**
 * @author Emre on 18.03.18
 */
public class UserInfoFragment extends BaseFragment<UserDetailsViewModel, FragmentUserInfoBinding> {

    private FragmentUserInfoBinding binding;

    public static UserInfoFragment newInstance() {
        return new UserInfoFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        getViewModel().user.observe(this, userResource -> {
            if (userResource.getData() != null) {
                binding.setUser(userResource.getData());
            }
        });
        getViewModel().userExistsInDb.observe(this, userExists -> {
            if (!userExists) {
                binding.fabFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_border_24dp));
                binding.fabFavorite.setOnClickListener(v -> getViewModel().insertUserAsFavorite());
            } else {
                binding.fabFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp));
                binding.fabFavorite.setOnClickListener(v -> getViewModel().removeUserFromFavorites());
            }
        });

        getTracker().setScreenName(UserInfoFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user_info;
    }

    @Override
    public UserDetailsViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(UserDetailsViewModel.class);
    }

}

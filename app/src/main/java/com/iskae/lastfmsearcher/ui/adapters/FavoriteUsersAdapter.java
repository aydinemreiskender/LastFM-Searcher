package com.iskae.lastfmsearcher.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.iskae.lastfmsearcher.core.data.model.User;
import com.iskae.lastfmsearcher.databinding.FavoriteUserListItemBinding;
import com.iskae.lastfmsearcher.ui.userdetails.UserDetailsActivity;

import java.util.List;

public class FavoriteUsersAdapter extends RecyclerView.Adapter<FavoriteUsersAdapter.ViewHolder> {
    private Context context;
    private List<User> users;

    public FavoriteUsersAdapter(Context context, List<User> users) {
        this.context = context;
        this.users = users;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FavoriteUserListItemBinding binding = FavoriteUserListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FavoriteUsersAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = users.get(position);
        if (user != null) {
            holder.binding.setUser(user);
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, UserDetailsActivity.class);
                intent.putExtra(UserDetailsActivity.USER_ID, user.getName());
                context.startActivity(intent);
            });
        }
    }


    @Override
    public int getItemCount() {
        if (users == null) return 0;
        return users.size();
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final FavoriteUserListItemBinding binding;

        public ViewHolder(FavoriteUserListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}

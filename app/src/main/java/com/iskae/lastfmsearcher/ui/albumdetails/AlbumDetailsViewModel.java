package com.iskae.lastfmsearcher.ui.albumdetails;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseViewModel;
import com.iskae.lastfmsearcher.core.data.LastFmRepository;
import com.iskae.lastfmsearcher.core.data.model.Album;
import com.iskae.lastfmsearcher.core.data.model.Resource;
import com.iskae.lastfmsearcher.core.data.model.Status;
import com.iskae.lastfmsearcher.util.Utils;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AlbumDetailsViewModel extends BaseViewModel {

    final MutableLiveData<Resource<Album>> albumResource = new MutableLiveData<>();

    @Inject
    public AlbumDetailsViewModel(Application application, LastFmRepository repository) {
        super(application, repository);
    }

    public void loadAlbumDetails(String artistName, String albumName) {
        getCompositeDisposable().add(getLastFmRepository().getAlbumDetails(artistName, albumName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> albumResource.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(albumResponse -> {
                    if (albumResponse != null) {
                        if (albumResponse.body() != null) {
                            albumResource.postValue(new Resource<>(albumResponse.body().getAlbum(), Status.SUCCESS, null));
                        } else {
                            albumResource.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(albumResponse.errorBody())));
                        }
                    }
                }, throwable -> albumResource.postValue(new Resource<>(null, Status.ERROR, getApplication().getString(R.string.error_network_connection)))));
    }
}

package com.iskae.lastfmsearcher.ui.favorites;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.iskae.lastfmsearcher.core.base.BaseViewModel;
import com.iskae.lastfmsearcher.core.data.LastFmRepository;
import com.iskae.lastfmsearcher.core.data.model.Resource;
import com.iskae.lastfmsearcher.core.data.model.Status;
import com.iskae.lastfmsearcher.core.data.model.User;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FavoritesViewModel extends BaseViewModel {
    final MutableLiveData<Resource<List<User>>> favoriteUsers = new MutableLiveData<>();

    @Inject
    public FavoritesViewModel(Application application, LastFmRepository repository) {
        super(application, repository);
    }

    public void loadFavoriteUsers() {
        getCompositeDisposable().add(getLastFmRepository().getAllFavoriteUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> favoriteUsers.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(users -> favoriteUsers.postValue(new Resource<>(users, Status.SUCCESS, null)),
                        throwable -> favoriteUsers.postValue(new Resource<>(null, Status.ERROR, throwable.getMessage()))));
    }
}

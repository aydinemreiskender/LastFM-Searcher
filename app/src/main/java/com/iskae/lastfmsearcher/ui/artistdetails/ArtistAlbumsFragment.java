package com.iskae.lastfmsearcher.ui.artistdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentArtistAlbumsBinding;
import com.iskae.lastfmsearcher.ui.adapters.AlbumsAdapter;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

import java.util.ArrayList;

import static com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity.ALBUM_NAME;
import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;

public class ArtistAlbumsFragment extends BaseFragment<ArtistViewModel, FragmentArtistAlbumsBinding> implements AlbumsAdapter.OnAlbumClickListener {

    private AlbumsAdapter albumsAdapter;

    public static ArtistAlbumsFragment newInstance() {
        return new ArtistAlbumsFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentArtistAlbumsBinding binding = getViewDataBinding();
        albumsAdapter = new AlbumsAdapter(getContext(), new ArrayList<>(), this);
        RecyclerView.LayoutManager artistTopAlbumsLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.rvArtistAlbums.setLayoutManager(artistTopAlbumsLayoutManager);
        binding.rvArtistAlbums.setItemAnimator(new DefaultItemAnimator());
        binding.rvArtistAlbums.setAdapter(albumsAdapter);

        getViewModel().artistTopAlbumsResource.observe(this, topAlbums -> {
            binding.setAlbumsResource(topAlbums);
            albumsAdapter.setAlbums(topAlbums.getData());
        });
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_artist_albums;
    }

    @Override
    public ArtistViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(ArtistViewModel.class);
    }

    @Override
    public void onAlbumClick(String albumName) {
        Intent intent = new Intent(getContext(), AlbumDetailsActivity.class);
        intent.putExtra(ARTIST_NAME, getViewModel().getArtistName());
        intent.putExtra(ALBUM_NAME, albumName);
        startActivity(intent);
    }
}

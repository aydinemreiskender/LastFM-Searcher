package com.iskae.lastfmsearcher.ui.userdetails;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentUserDetailsBinding;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

import static com.iskae.lastfmsearcher.ui.userdetails.UserDetailsActivity.USER_ID;

public class UserDetailsFragment extends BaseFragment<UserDetailsViewModel, FragmentUserDetailsBinding> {

    private FragmentUserDetailsBinding binding;

    public static UserDetailsFragment newInstance(String userId) {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                String userId = getArguments().getString(USER_ID);
                if (userId != null) {
                    setTitle(userId);
                    getViewModel().setUserId(userId);

                }
            }
        }
        UserDetailsPagerAdapter pagerAdapter = new UserDetailsPagerAdapter(getContext(), getChildFragmentManager());
        binding.userDetailsPager.setAdapter(pagerAdapter);
        binding.userDetailsPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getViewModel().currentTab.setValue(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        binding.userDetailsTabs.setupWithViewPager(binding.userDetailsPager);
        getViewModel().user.observe(this, user -> binding.setUserResource(user));
        getViewModel().currentTab.observe(this, binding.userDetailsPager::setCurrentItem);
        getTracker().setScreenName(UserDetailsFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user_details;
    }

    @Override
    public UserDetailsViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(UserDetailsViewModel.class);
    }
}

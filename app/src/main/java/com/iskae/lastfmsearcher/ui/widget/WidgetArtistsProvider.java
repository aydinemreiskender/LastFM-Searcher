package com.iskae.lastfmsearcher.ui.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.data.model.Artist;
import com.iskae.lastfmsearcher.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;

public class WidgetArtistsProvider implements RemoteViewsService.RemoteViewsFactory {

    private List<Artist> artistsList;
    private Context context;
    private int appWidgetId;

    WidgetArtistsProvider(Context context, Intent intent) {
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        artistsList = WidgetFetchDataService.artistsList;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {
        artistsList.clear();
    }

    @Override
    public int getCount() {
        return artistsList != null ? artistsList.size() : 0;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        if (position >= artistsList.size()) {
            return null;
        }
        RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.widget_artists_list_item);
        Artist artist = artistsList.get(position);
        Bitmap bitmap = null;
        try {
            bitmap = Picasso.get().load(ImageUtils.getSuitableImageUrl(artist.getImages())).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bitmap != null) {
            remoteView.setImageViewBitmap(R.id.imageView, bitmap);
        } else {
            remoteView.setImageViewResource(R.id.imageView, R.drawable.ic_logo);
        }
        remoteView.setTextViewText(R.id.tvArtistRank, String.valueOf(position + 1));
        remoteView.setTextViewText(R.id.tvArtistName, artist.getArtistName());

        Intent intent = new Intent();
        intent.putExtra(ARTIST_NAME, artist.getArtistName());
        Bundle extras = new Bundle();
        extras.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.putExtras(extras);

        remoteView.setOnClickFillInIntent(R.id.artistContainer, intent);
        return remoteView;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


}

package com.iskae.lastfmsearcher.ui.artistdetails;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ArtistDetailsModule {

    @ContributesAndroidInjector
    abstract ArtistDetailsFragment contributesArtistDetailsFragment();

    @ContributesAndroidInjector
    abstract ArtistInfoFragment contributesArtistInfoFragment();

    @ContributesAndroidInjector
    abstract ArtistTracksFragment contributesArtistTracksFragment();

    @ContributesAndroidInjector
    abstract ArtistAlbumsFragment contributesArtistAlbumFragment();

}

package com.iskae.lastfmsearcher.ui.widget;

import android.content.SharedPreferences;

import com.iskae.lastfmsearcher.core.data.LastFmRepository;
import com.iskae.lastfmsearcher.core.data.model.response.TopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TopTracksResponse;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;

public class WidgetUtils {
    public static final String PREF_ARTISTS = "PREF_ARTISTS";
    public static final String PREF_TRACKS = "PREF_TRACKS";

    private final SharedPreferences preferences;
    private final LastFmRepository repository;

    @Inject
    public WidgetUtils(SharedPreferences preferences, LastFmRepository repository) {
        this.preferences = preferences;
        this.repository = repository;
    }

    public Single<Response<TopArtistsResponse>> getWorldTopArtistsList() {
        return repository.getWorldTopArtistsList();
    }

    public Single<Response<TopTracksResponse>> getWorldTopTracksList() {
        return repository.getWorldTopTracksList();
    }

    public String getWidgetItemType(int appWidgetId){
        return preferences.getString(String.valueOf(appWidgetId),null);
    }
}

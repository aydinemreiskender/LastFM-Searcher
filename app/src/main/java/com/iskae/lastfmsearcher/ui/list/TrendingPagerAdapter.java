package com.iskae.lastfmsearcher.ui.list;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.iskae.lastfmsearcher.R;


/**
 * @author Emre on 17.03.18
 */

public class TrendingPagerAdapter extends FragmentPagerAdapter {
    private static final int NUM_ITEMS = 2;

    private Context context;

    public TrendingPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return WorldTrendingFragment.newInstance();
            case 1:
                return LocationTrendingFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.tab_world);
            case 1:
                return context.getString(R.string.tab_location);
            default:
                return null;
        }
    }

}

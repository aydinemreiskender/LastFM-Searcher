package com.iskae.lastfmsearcher.ui.widget;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

public class UpdateWidgetService extends IntentService {

    public UpdateWidgetService() {
        super("UpdateWidgetService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            LastFmSearcherWidgetProvider.updateWidget(this, intent);
        }
    }
}

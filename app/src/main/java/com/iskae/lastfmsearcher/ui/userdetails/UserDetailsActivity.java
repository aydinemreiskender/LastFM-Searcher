package com.iskae.lastfmsearcher.ui.userdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseActivity;
import com.iskae.lastfmsearcher.databinding.ActivityMasterBinding;

/**
 * @author Emre on 18.03.18
 */
public class UserDetailsActivity extends BaseActivity<UserDetailsViewModel, ActivityMasterBinding> {
    public static final String USER_ID = "USER_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMasterBinding binding = getViewDataBinding();
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        String userId = getIntent().getStringExtra(USER_ID);
        if (userId != null) {
            addFragmentToActivity(getSupportFragmentManager(), UserDetailsFragment.newInstance(userId),
                    R.id.contentContainer, UserDetailsFragment.class.getSimpleName());
        }
    }

    @Override
    public UserDetailsViewModel getViewModel() {
        return ViewModelProviders.of(this, viewModelFactory).get(UserDetailsViewModel.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_master;
    }

}

package com.iskae.lastfmsearcher.ui.userdetails;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.iskae.lastfmsearcher.R;

/**
 * @author Emre on 18.03.18
 */

public class UserDetailsPagerAdapter extends FragmentPagerAdapter {
    private static final int USER_DETAILS_TAB_COUNT = 3;

    private Context context;

    public UserDetailsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return UserInfoFragment.newInstance();
            case 1:
                return UserTopArtistsFragment.newInstance();
            case 2:
                return UserTopTracksFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return USER_DETAILS_TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.tab_user_info);
            case 1:
                return context.getString(R.string.tab_top_artists);
            case 2:
                return context.getString(R.string.tab_top_tracks);
            default:
                return null;
        }
    }
}

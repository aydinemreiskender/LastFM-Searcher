package com.iskae.lastfmsearcher.ui.albumdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseActivity;
import com.iskae.lastfmsearcher.databinding.ActivityMasterBinding;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;

public class AlbumDetailsActivity extends BaseActivity<AlbumDetailsViewModel, ActivityMasterBinding> {

    public static final String ALBUM_NAME = "ALBUM_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMasterBinding binding = getViewDataBinding();
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        String artistName = getIntent().getStringExtra(ARTIST_NAME);
        String albumName = getIntent().getStringExtra(ALBUM_NAME);
        if (albumName != null) {
            addFragmentToActivity(getSupportFragmentManager(), AlbumDetailsFragment.newInstance(artistName, albumName),
                    R.id.contentContainer, AlbumDetailsFragment.class.getSimpleName());
        }
    }

    @Override
    public AlbumDetailsViewModel getViewModel() {
        return ViewModelProviders.of(this, viewModelFactory).get(AlbumDetailsViewModel.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_master;
    }
}

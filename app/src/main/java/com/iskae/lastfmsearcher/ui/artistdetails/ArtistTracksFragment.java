package com.iskae.lastfmsearcher.ui.artistdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.core.data.model.Track;
import com.iskae.lastfmsearcher.databinding.FragmentArtistTracksBinding;
import com.iskae.lastfmsearcher.ui.adapters.ExpandedTracksAdapter;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

import java.util.ArrayList;

public class ArtistTracksFragment extends BaseFragment<ArtistViewModel, FragmentArtistTracksBinding> {

    private ExpandedTracksAdapter tracksAdapter;

    public static ArtistTracksFragment newInstance() {
        return new ArtistTracksFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentArtistTracksBinding binding = getViewDataBinding();
        tracksAdapter = new ExpandedTracksAdapter(getContext(), new ArrayList<Track>());
        RecyclerView.LayoutManager artistTopTracksLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.rvArtistTracks.setLayoutManager(artistTopTracksLayoutManager);
        binding.rvArtistTracks.setItemAnimator(new DefaultItemAnimator());
        binding.rvArtistTracks.setAdapter(tracksAdapter);

        getViewModel().artistTopTracksResource.observe(this, topTracksResource -> {
            binding.setArtistTracksResource(topTracksResource);
            tracksAdapter.setTracks(topTracksResource.getData());
        });
        getTracker().setScreenName(ArtistTracksFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_artist_tracks;
    }

    @Override
    public ArtistViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(ArtistViewModel.class);
    }

}

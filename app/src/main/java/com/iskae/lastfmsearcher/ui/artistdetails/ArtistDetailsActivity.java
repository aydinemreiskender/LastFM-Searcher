package com.iskae.lastfmsearcher.ui.artistdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseActivity;
import com.iskae.lastfmsearcher.databinding.ActivityMasterBinding;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

public class ArtistDetailsActivity extends BaseActivity<ArtistViewModel, ActivityMasterBinding> {
    public static final String ARTIST_NAME = "ARTIST_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMasterBinding binding = getViewDataBinding();
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        String artistName = getIntent().getStringExtra(ARTIST_NAME);
        if (artistName != null) {
            addFragmentToActivity(getSupportFragmentManager(), ArtistDetailsFragment.newInstance(artistName),
                    R.id.contentContainer, ArtistDetailsFragment.class.getSimpleName());
        }
    }

    @Override
    public ArtistViewModel getViewModel() {
        return ViewModelProviders.of(this, viewModelFactory).get(ArtistViewModel.class);
    }

    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_master;
    }

}

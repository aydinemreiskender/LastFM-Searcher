package com.iskae.lastfmsearcher.ui.list;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.location.Address;
import android.location.Geocoder;

import com.iskae.lastfmsearcher.core.base.BaseViewModel;
import com.iskae.lastfmsearcher.core.data.LastFmRepository;
import com.iskae.lastfmsearcher.core.data.model.Artist;
import com.iskae.lastfmsearcher.core.data.model.Resource;
import com.iskae.lastfmsearcher.core.data.model.Status;
import com.iskae.lastfmsearcher.core.data.model.Track;
import com.iskae.lastfmsearcher.util.Utils;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Emre on 17.03.18
 */

public class ListViewModel extends BaseViewModel {
    final MutableLiveData<Resource<List<Artist>>> worldTrendingArtists = new MutableLiveData<>();
    final MutableLiveData<Resource<List<Track>>> worldTrendingTracks = new MutableLiveData<>();
    final MutableLiveData<Resource<List<Artist>>> locationTrendingArtists = new MutableLiveData<>();
    final MutableLiveData<Resource<List<Track>>> locationTrendingTracks = new MutableLiveData<>();

    final MutableLiveData<Resource> worldErrorResource = new MutableLiveData<>();
    final MutableLiveData<Resource> locationErrorResource = new MutableLiveData<>();
    final MutableLiveData<Resource<String>> currentCountry = new MutableLiveData<>();
    final MutableLiveData<Integer> currentTab = new MutableLiveData<>();
    String searchQuery;

    @Inject
    public ListViewModel(Application application, LastFmRepository repository) {
        super(application, repository);
    }

    public void loadWorldTrendingArtistsList() {
        getCompositeDisposable().add(getLastFmRepository().getWorldTopArtistsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> worldTrendingArtists.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(chartTopArtistsResponse -> {
                    if (chartTopArtistsResponse != null) {
                        if (chartTopArtistsResponse.body() != null &&
                                chartTopArtistsResponse.body().getArtistsResponse().getArtists() != null) {
                            worldTrendingArtists.postValue(new Resource<>(chartTopArtistsResponse.body().getArtistsResponse().getArtists(),
                                    Status.SUCCESS, null));
                        } else {
                            worldTrendingArtists.postValue(new Resource<>(null,
                                    Status.ERROR, Utils.getErrorMessage(chartTopArtistsResponse.errorBody())));
                        }
                    }
                }, throwable -> worldTrendingArtists.postValue(new Resource<>(null,
                        Status.ERROR, throwable.getMessage()))));

    }

    public void loadWorldTrendingTracksList() {
        getCompositeDisposable().add(getLastFmRepository().getWorldTopTracksList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> worldTrendingTracks.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(chartTopTracksResponse -> {
                    if (chartTopTracksResponse != null) {
                        if (chartTopTracksResponse.body() != null &&
                                chartTopTracksResponse.body().getTracksResponse().getTracks() != null) {
                            worldTrendingTracks.postValue(new Resource<>(chartTopTracksResponse.body().getTracksResponse().getTracks(),
                                    Status.SUCCESS, null));
                        } else {
                            worldTrendingTracks.postValue(new Resource<>(null,
                                    Status.ERROR, Utils.getErrorMessage(chartTopTracksResponse.errorBody())));
                        }
                    }
                }, throwable -> worldTrendingTracks.postValue(new Resource<>(null,
                        Status.ERROR, throwable.getMessage()))));
    }

    public void loadGeoTrendingArtistsList(String location) {
        getCompositeDisposable().add(getLastFmRepository().getGeoTopArtistsList(location)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> locationTrendingArtists.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(chartTopArtistsResponse -> {
                    if (chartTopArtistsResponse != null) {
                        if (chartTopArtistsResponse.body() != null &&
                                chartTopArtistsResponse.body().getArtistsResponse().getArtists() != null) {
                            locationTrendingArtists.postValue(new Resource<>(chartTopArtistsResponse.body().getArtistsResponse().getArtists(),
                                    Status.SUCCESS, null));
                        } else {
                            locationTrendingArtists.postValue(new Resource<>(null,
                                    Status.ERROR, Utils.getErrorMessage(chartTopArtistsResponse.errorBody())));
                        }
                    }
                }, throwable -> locationTrendingArtists.postValue(new Resource<>(null,
                        Status.ERROR, throwable.getMessage()))));
    }

    public void loadGeoTrendingTracksList(String location) {
        getCompositeDisposable().add(getLastFmRepository().getGeoTopTracksList(location)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> locationTrendingTracks.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(chartTopTracksResponse -> {
                    if (chartTopTracksResponse != null) {
                        if (chartTopTracksResponse.body() != null &&
                                chartTopTracksResponse.body().getTracksResponse().getTracks() != null) {
                            locationTrendingTracks.postValue(new Resource<>(chartTopTracksResponse.body().getTracksResponse().getTracks(),
                                    Status.SUCCESS, null));
                        } else {
                            locationTrendingTracks.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(chartTopTracksResponse.errorBody())));
                        }
                    }
                }, throwable -> locationTrendingTracks.postValue(new Resource<>(null, Status.ERROR, throwable.getMessage()))));
    }

    public void loadCurrentCountry(Geocoder geocoder, double latitude, double longitude) {
        Single.fromCallable(() -> getCountry(geocoder, latitude, longitude))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(String s) {
                        if (currentCountry.getValue() == null || !(currentCountry.getValue() != null
                                && currentCountry.getValue().getStatus().name().equals(s))) {
                            currentCountry.postValue(new Resource<>(s, Status.SUCCESS, null));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        currentCountry.postValue(new Resource<>(null, Status.ERROR, e.getMessage()));
                    }
                });

    }

    private String getCountry(Geocoder geocoder, double latitude, double longitude) throws IOException {
        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
        if (addresses != null && addresses.get(0) != null) {
            return addresses.get(0).getCountryName();
        }
        return null;
    }

}

package com.iskae.lastfmsearcher.ui.trackdetails;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseViewModel;
import com.iskae.lastfmsearcher.core.data.LastFmRepository;
import com.iskae.lastfmsearcher.core.data.model.Resource;
import com.iskae.lastfmsearcher.core.data.model.Status;
import com.iskae.lastfmsearcher.core.data.model.Track;
import com.iskae.lastfmsearcher.util.Utils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TrackDetailsViewModel extends BaseViewModel {

    final MutableLiveData<Resource<Track>> trackResource = new MutableLiveData<>();
    final MutableLiveData<Resource<List<Track>>> similarTracksResource = new MutableLiveData<>();

    @Inject
    public TrackDetailsViewModel(Application application, LastFmRepository repository) {
        super(application, repository);
    }

    public void loadTrackInfo(String artistName, String trackName) {
        getCompositeDisposable().add(getLastFmRepository().getTrackDetails(artistName, trackName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> trackResource.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(trackResponse -> {
                    if (trackResponse != null) {
                        if (trackResponse.body() != null) {
                            trackResource.postValue(new Resource<>(trackResponse.body().getTrack(), Status.SUCCESS, null));
                        } else {
                            trackResource.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(trackResponse.errorBody())));
                        }
                    }
                }, throwable -> trackResource.postValue(new Resource<>(null, Status.ERROR, getApplication().getString(R.string.error_network_connection)))));
    }

    public void loadSimilarTracks(String artistName, String trackName) {
        getCompositeDisposable().add(getLastFmRepository().getSimilarTracks(artistName, trackName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> similarTracksResource.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(similarTracksResponse -> {
                    if (similarTracksResponse != null) {
                        if (similarTracksResponse.body() != null) {
                            similarTracksResource.postValue(new Resource<>(similarTracksResponse.body().getSimilarTracksResponse().getTracks(), Status.SUCCESS, null));
                        } else {
                            similarTracksResource.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(similarTracksResponse.errorBody())));
                        }
                    }
                }, throwable -> similarTracksResource.postValue(new Resource<>(null, Status.ERROR, getApplication().getString(R.string.error_network_connection)))));
    }
}

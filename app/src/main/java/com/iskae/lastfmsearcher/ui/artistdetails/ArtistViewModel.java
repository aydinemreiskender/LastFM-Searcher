package com.iskae.lastfmsearcher.ui.artistdetails;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseViewModel;
import com.iskae.lastfmsearcher.core.data.LastFmRepository;
import com.iskae.lastfmsearcher.core.data.model.Album;
import com.iskae.lastfmsearcher.core.data.model.Artist;
import com.iskae.lastfmsearcher.core.data.model.Resource;
import com.iskae.lastfmsearcher.core.data.model.Status;
import com.iskae.lastfmsearcher.core.data.model.Track;
import com.iskae.lastfmsearcher.util.Utils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ArtistViewModel extends BaseViewModel {

    final MutableLiveData<Resource<Artist>> artistResource = new MutableLiveData<>();
    final MutableLiveData<Resource<List<Artist>>> similarArtistsResource = new MutableLiveData<>();
    final MutableLiveData<Resource<List<Album>>> artistTopAlbumsResource = new MutableLiveData<>();
    final MutableLiveData<Resource<List<Track>>> artistTopTracksResource = new MutableLiveData<>();
    final MutableLiveData<Integer> currentTab = new MutableLiveData<>();
    private String artistName;

    @Inject
    public ArtistViewModel(Application application, LastFmRepository repository) {
        super(application, repository);
    }

    public void loadArtist() {
        getCompositeDisposable().add(getLastFmRepository().getArtistInfo(artistName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> artistResource.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(artistResponse -> {
                    if (artistResponse != null) {
                        if (artistResponse.body() != null) {
                            artistResource.postValue(new Resource<>(artistResponse.body().getArtist(), Status.SUCCESS, null));
                        } else {
                            artistResource.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(artistResponse.errorBody())));
                        }
                    }
                }, throwable -> artistResource.postValue(new Resource<>(null, Status.ERROR, getApplication().getString(R.string.error_network_connection)))));


    }

    public void loadSimilarArtists() {
        getCompositeDisposable().add(getLastFmRepository().getSimilarArtists(artistName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> similarArtistsResource.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(similarArtistsResponse -> {
                    if (similarArtistsResponse != null) {
                        if (similarArtistsResponse.body() != null) {
                            similarArtistsResource.postValue(new Resource<>(similarArtistsResponse.body().getSimilarArtists().getArtists(), Status.SUCCESS, null));
                        } else {
                            similarArtistsResource.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(similarArtistsResponse.errorBody())));
                        }
                    }
                }, throwable -> similarArtistsResource.postValue(new Resource<>(null, Status.ERROR, getApplication().getString(R.string.error_network_connection)))));
    }

    public void loadArtistTopAlbums() {
        getCompositeDisposable().add(getLastFmRepository().getArtistTopAlbums(artistName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> artistTopAlbumsResource.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(albumsResponse -> {
                    if (albumsResponse != null) {
                        if (albumsResponse.body() != null) {
                            artistTopAlbumsResource.postValue(new Resource<>(albumsResponse.body().getAlbumsResponse().getAlbums(), Status.SUCCESS, null));
                        } else {
                            artistTopAlbumsResource.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(albumsResponse.errorBody())));
                        }
                    }
                }, throwable -> artistTopAlbumsResource.postValue(new Resource<>(null, Status.ERROR, getApplication().getString(R.string.error_network_connection)))));
    }

    public void loadArtistTopTracks() {
        getCompositeDisposable().add(getLastFmRepository().getArtistTopTracks(artistName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> artistTopTracksResource.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(tracksResponse -> {
                    if (tracksResponse != null) {
                        if (tracksResponse.body() != null) {
                            artistTopTracksResource.postValue(new Resource<>(tracksResponse.body().getTracksResponse().getTracks(), Status.SUCCESS, null));
                        } else {
                            artistTopTracksResource.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(tracksResponse.errorBody())));
                        }
                    }
                }, throwable -> artistTopTracksResource.postValue(new Resource<>(null, Status.ERROR, getApplication().getString(R.string.error_network_connection)))));
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
        loadArtist();
        loadSimilarArtists();
        loadArtistTopAlbums();
        loadArtistTopTracks();
    }

}

package com.iskae.lastfmsearcher.ui.artistdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentArtistDetailsBinding;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsFragment;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;

public class ArtistDetailsFragment extends BaseFragment<ArtistViewModel, FragmentArtistDetailsBinding> {

    public static ArtistDetailsFragment newInstance(String artistId) {
        ArtistDetailsFragment fragment = new ArtistDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARTIST_NAME, artistId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentArtistDetailsBinding binding = getViewDataBinding();
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                String artistId = getArguments().getString(ARTIST_NAME);
                if (artistId != null) {
                    getViewModel().setArtistName(artistId);
                }
            }
        }
        ArtistDetailsPagerAdapter pagerAdapter = new ArtistDetailsPagerAdapter(getContext(), getChildFragmentManager());
        binding.artistDetailsPager.setAdapter(pagerAdapter);
        binding.artistDetailsPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getViewModel().currentTab.setValue(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        binding.artistDetailsTabs.setupWithViewPager(binding.artistDetailsPager);
        getViewModel().currentTab.observe(this, binding.artistDetailsPager::setCurrentItem);
        getTracker().setScreenName(ArtistDetailsFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_artist_details;
    }

    @Override
    public ArtistViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(ArtistViewModel.class);
    }
}

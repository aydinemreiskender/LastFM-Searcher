package com.iskae.lastfmsearcher.ui.list;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.core.data.model.Resource;
import com.iskae.lastfmsearcher.core.data.model.Status;
import com.iskae.lastfmsearcher.databinding.FragmentTrendingBinding;
import com.iskae.lastfmsearcher.ui.adapters.ArtistsAdapter;
import com.iskae.lastfmsearcher.ui.adapters.TracksAdapter;
import com.iskae.lastfmsearcher.util.PermissionUtil;
import com.iskae.lastfmsearcher.util.Utils;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * @author Emre on 18.03.18
 */

public class LocationTrendingFragment extends BaseFragment<ListViewModel, FragmentTrendingBinding> {
    private static final int REQUEST_LOCATION_PERMISSION = 41232;

    private FragmentTrendingBinding binding;
    private ArtistsAdapter artistsAdapter;
    private TracksAdapter tracksAdapter;
    private FusedLocationProviderClient fusedLocationClient;

    public static LocationTrendingFragment newInstance() {
        return new LocationTrendingFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        artistsAdapter = new ArtistsAdapter(getContext(), new ArrayList<>());
        tracksAdapter = new TracksAdapter(getContext(), new ArrayList<>());
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        RecyclerView.LayoutManager artistsLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvTrendingArtists.setLayoutManager(artistsLayoutManager);
        binding.rvTrendingArtists.setItemAnimator(new DefaultItemAnimator());
        binding.rvTrendingArtists.setAdapter(artistsAdapter);

        RecyclerView.LayoutManager tracksLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvTrendingTracks.setLayoutManager(tracksLayoutManager);
        binding.rvTrendingTracks.setItemAnimator(new DefaultItemAnimator());
        binding.rvTrendingTracks.setAdapter(tracksAdapter);

        binding.trendingContainer.setVisibility(View.GONE);

        getViewModel().locationTrendingArtists.observe(this, artistsResource -> {
            binding.setArtistsResource(artistsResource);
            artistsAdapter.setArtists(artistsResource.getData());
        });

        getViewModel().locationTrendingTracks.observe(this, tracksResource -> {
            binding.setTracksResource(tracksResource);
            tracksAdapter.setTracks(tracksResource.getData());
        });

        getViewModel().currentCountry.observe(this, countryResource -> {
            if (Utils.isNetworkAvailable(getContext())) {
                binding.setErrorResource(null);
                if (artistsAdapter.getItemCount() == 0 || tracksAdapter.getItemCount() == 0) {
                    getViewModel().loadGeoTrendingArtistsList(countryResource.getData());
                    getViewModel().loadGeoTrendingTracksList(countryResource.getData());
                }
            } else {
                getViewModel().locationErrorResource.setValue(new Resource<>(null, Status.ERROR, getString(R.string.error_network_connection)));
            }
        });

        getViewModel().locationErrorResource.observe(this, errorResource -> binding.setErrorResource(errorResource));
        getTracker().setScreenName(LocationTrendingFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !PermissionUtil.hasAccessLocationPermissions(getContext())) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
            } else {
                requestLocation();
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void requestLocation() {
        LocationRequest locationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        fusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location location = locationResult.getLastLocation();
                if (location != null && getViewModel() != null) {
                    getViewModel().loadCurrentCountry(new Geocoder(getContext()), location.getLatitude(), location.getLongitude());
                }
            }
        }, Looper.myLooper())
                .addOnFailureListener(Timber::e)
                .addOnCompleteListener(task -> Timber.d("Fetching location is completed"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (REQUEST_LOCATION_PERMISSION == requestCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocation();
            } else {
                getViewModel().locationErrorResource.setValue(new Resource<>(null, Status.ERROR, getString(R.string.error_location)));
            }
        }
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_trending;
    }

    @Override
    public ListViewModel getViewModel() {
        if (getActivity() != null)
            return ViewModelProviders.of(getActivity(), viewModelFactory).get(ListViewModel.class);
        return null;
    }
}

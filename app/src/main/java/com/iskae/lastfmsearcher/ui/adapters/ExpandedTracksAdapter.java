package com.iskae.lastfmsearcher.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iskae.lastfmsearcher.core.data.model.Track;
import com.iskae.lastfmsearcher.databinding.TracksListItemExpandedBinding;
import com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsActivity;
import com.iskae.lastfmsearcher.util.ImageUtils;

import java.util.List;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;
import static com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsActivity.TRACK_NAME;

/**
 * @author Emre on 18.03.18
 */

public class ExpandedTracksAdapter extends RecyclerView.Adapter<ExpandedTracksAdapter.ViewHolder> {
    private Context context;
    private List<Track> tracks;

    public ExpandedTracksAdapter(Context context, List<Track> tracks) {
        this.context = context;
        this.tracks = tracks;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TracksListItemExpandedBinding binding = TracksListItemExpandedBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ExpandedTracksAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Track track = tracks.get(position);
        if (track != null) {
            holder.binding.setTrack(track);
            String imageUrl = ImageUtils.getSuitableImageUrl(track.getImages());
            if (imageUrl != null && imageUrl.length() > 0) {
                holder.binding.imageView.setVisibility(View.VISIBLE);
                holder.binding.tvArtistName.setVisibility(View.VISIBLE);
            } else {
                holder.binding.imageView.setVisibility(View.GONE);
                holder.binding.tvArtistName.setVisibility(View.GONE);
            }

            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, TrackDetailsActivity.class);
                intent.putExtra(ARTIST_NAME, track.getArtist().getArtistName());
                intent.putExtra(TRACK_NAME, track.getName());
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        if (tracks == null) return 0;
        return tracks.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TracksListItemExpandedBinding binding;

        public ViewHolder(TracksListItemExpandedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}

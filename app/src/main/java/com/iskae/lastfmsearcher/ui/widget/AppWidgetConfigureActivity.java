package com.iskae.lastfmsearcher.ui.widget;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.databinding.ActivityWidgetConfigBinding;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

import static com.iskae.lastfmsearcher.ui.widget.WidgetUtils.PREF_ARTISTS;
import static com.iskae.lastfmsearcher.ui.widget.WidgetUtils.PREF_TRACKS;

public class AppWidgetConfigureActivity extends AppCompatActivity {

    private int appWidgetId;

    @Inject
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ActivityWidgetConfigBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_widget_config);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            appWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
        binding.trendingArtistsButton.setOnClickListener(v -> {
            preferences.edit().putString(String.valueOf(appWidgetId), PREF_ARTISTS).apply();
            startWidget();
        });

        binding.trendingTracksButton.setOnClickListener(v -> {
            preferences.edit().putString(String.valueOf(appWidgetId), PREF_TRACKS).apply();
            startWidget();
        });
    }

    private void startWidget() {
        Intent intent = new Intent();
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        setResult(RESULT_OK, intent);
        Intent serviceIntent = new Intent(this, WidgetFetchDataService.class);
        serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        startService(serviceIntent);
        finish();
    }
}

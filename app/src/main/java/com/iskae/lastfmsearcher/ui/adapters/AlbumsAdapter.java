package com.iskae.lastfmsearcher.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.data.model.Album;
import com.iskae.lastfmsearcher.databinding.AlbumListItemBinding;

import java.util.List;

public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.ViewHolder> {
    private Context context;
    private List<Album> albums;
    private OnAlbumClickListener onAlbumClickListener;

    public AlbumsAdapter(Context context, List<Album> albums, OnAlbumClickListener onAlbumClickListener) {
        this.context = context;
        this.albums = albums;
        this.onAlbumClickListener = onAlbumClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AlbumListItemBinding binding = AlbumListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new AlbumsAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Album album = albums.get(position);
        if (album != null) {
            holder.binding.setAlbum(album);
            holder.binding.tvAlbumRank.setText(context.getString(R.string.rank_number, String.valueOf(position + 1)));
            holder.itemView.setOnClickListener(v -> onAlbumClickListener.onAlbumClick(album.getName()));
        }
    }


    @Override
    public int getItemCount() {
        if (albums == null) return 0;
        return albums.size();
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
        notifyDataSetChanged();
    }

    public interface OnAlbumClickListener {
        void onAlbumClick(String albumName);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final AlbumListItemBinding binding;

        public ViewHolder(AlbumListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
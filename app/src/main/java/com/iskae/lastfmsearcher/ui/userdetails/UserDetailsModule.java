package com.iskae.lastfmsearcher.ui.userdetails;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author Emre on 18.03.18
 */

@Module
public abstract class UserDetailsModule {

    @ContributesAndroidInjector
    abstract UserDetailsFragment contributesUserDetailsFragment();

    @ContributesAndroidInjector
    abstract UserInfoFragment contributesUserInfoFragment();

    @ContributesAndroidInjector
    abstract UserTopArtistsFragment contributesUserTopArtistsFragment();

    @ContributesAndroidInjector
    abstract UserTopTracksFragment contributesUserTopTracksFragment();
}

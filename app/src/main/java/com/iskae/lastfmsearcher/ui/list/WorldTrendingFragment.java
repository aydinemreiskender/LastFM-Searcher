package com.iskae.lastfmsearcher.ui.list;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.core.data.model.Resource;
import com.iskae.lastfmsearcher.core.data.model.Status;
import com.iskae.lastfmsearcher.databinding.FragmentTrendingBinding;
import com.iskae.lastfmsearcher.ui.adapters.ArtistsAdapter;
import com.iskae.lastfmsearcher.ui.adapters.TracksAdapter;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;
import com.iskae.lastfmsearcher.util.Utils;

import java.util.ArrayList;

/**
 * @author Emre on 18.03.18
 */

public class WorldTrendingFragment extends BaseFragment<ListViewModel, FragmentTrendingBinding> {

    private FragmentTrendingBinding binding;
    private ArtistsAdapter artistsAdapter;
    private TracksAdapter tracksAdapter;

    public static WorldTrendingFragment newInstance() {
        return new WorldTrendingFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        artistsAdapter = new ArtistsAdapter(getContext(), new ArrayList<>());
        tracksAdapter = new TracksAdapter(getContext(), new ArrayList<>());
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        RecyclerView.LayoutManager artistsLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvTrendingArtists.setLayoutManager(artistsLayoutManager);
        binding.rvTrendingArtists.setItemAnimator(new DefaultItemAnimator());
        binding.rvTrendingArtists.setAdapter(artistsAdapter);

        RecyclerView.LayoutManager tracksLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvTrendingTracks.setLayoutManager(tracksLayoutManager);
        binding.rvTrendingTracks.setItemAnimator(new DefaultItemAnimator());
        binding.rvTrendingTracks.setAdapter(tracksAdapter);

        getViewModel().worldTrendingArtists.observe(this, artistsResource -> {
            binding.setArtistsResource(artistsResource);
            artistsAdapter.setArtists(artistsResource.getData());
        });

        getViewModel().worldTrendingTracks.observe(this, tracksResource -> {
            binding.setTracksResource(tracksResource);
            tracksAdapter.setTracks(tracksResource.getData());
        });

        getViewModel().worldErrorResource.observe(this, errorResource -> binding.setErrorResource(errorResource));
        getTracker().setScreenName(WorldTrendingFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            if (Utils.isNetworkAvailable(getContext())) {
                getViewModel().worldErrorResource.setValue(null);
                getViewModel().loadWorldTrendingArtistsList();
                getViewModel().loadWorldTrendingTracksList();
            } else {
                getViewModel().worldErrorResource.setValue(new Resource(null, Status.ERROR, getString(R.string.error_network_connection)));
            }
        }
    }


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_trending;
    }

    @Override
    public ListViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(ListViewModel.class);
    }
}

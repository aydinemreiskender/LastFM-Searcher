package com.iskae.lastfmsearcher.ui.trackdetails;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TrackDetailsModule {

    @ContributesAndroidInjector
    abstract TrackDetailsFragment contributesTrackDetailsFragment();
}

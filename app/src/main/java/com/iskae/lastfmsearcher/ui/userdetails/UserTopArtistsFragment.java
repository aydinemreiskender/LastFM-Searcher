package com.iskae.lastfmsearcher.ui.userdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentUserTrendingBinding;
import com.iskae.lastfmsearcher.ui.adapters.ExpandedArtistsAdapter;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

import java.util.ArrayList;

/**
 * @author Emre on 18.03.18
 */

public class UserTopArtistsFragment extends BaseFragment<UserDetailsViewModel, FragmentUserTrendingBinding> {

    private FragmentUserTrendingBinding binding;
    private ExpandedArtistsAdapter artistsAdapter;

    public static UserTopArtistsFragment newInstance() {
        return new UserTopArtistsFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        artistsAdapter = new ExpandedArtistsAdapter(getContext(), new ArrayList<>());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        binding.rvTrendingList.setLayoutManager(layoutManager);
        binding.rvTrendingList.setItemAnimator(new DefaultItemAnimator());
        binding.rvTrendingList.setAdapter(artistsAdapter);

        getViewModel().userTopArtists.observe(this, artistsResource -> {
            binding.setUserTrendingResource(artistsResource);
            artistsAdapter.setArtists(artistsResource.getData());
        });
        getTracker().setScreenName(UserTopArtistsFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user_trending;
    }

    @Override
    public UserDetailsViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(UserDetailsViewModel.class);
    }
}

package com.iskae.lastfmsearcher.ui.list;

import android.app.SearchManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseActivity;
import com.iskae.lastfmsearcher.databinding.ActivityListBinding;
import com.iskae.lastfmsearcher.ui.favorites.FavoritesActivity;
import com.iskae.lastfmsearcher.ui.userdetails.UserDetailsActivity;

/**
 * @author Emre on 17.03.18
 */

public class ListActivity extends BaseActivity<ListViewModel, ActivityListBinding> {

    private ActivityListBinding binding;
    private SearchView searchView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
        TrendingPagerAdapter adapter = new TrendingPagerAdapter(this, getSupportFragmentManager());
        setSupportActionBar(binding.toolbar);
        binding.trendingPager.setAdapter(adapter);
        binding.trendingPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getViewModel().currentTab.setValue(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        binding.trendingTabs.setupWithViewPager(binding.trendingPager);

        getViewModel().currentCountry.observe(this, currentCountryResource -> {
            TabLayout.Tab locationTab = binding.trendingTabs.getTabAt(1);
            if (locationTab != null) {
                locationTab.setText(currentCountryResource.getData());
            }
        });

        getViewModel().currentTab.observe(this, binding.trendingPager::setCurrentItem);
    }

    @Override
    public ListViewModel getViewModel() {
        return ViewModelProviders.of(this, viewModelFactory).get(ListViewModel.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_list;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(searchManager != null ? searchManager.getSearchableInfo(getComponentName()) : null);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        if (getViewModel().searchQuery != null && !getViewModel().searchQuery.isEmpty()) {
            searchView.setQuery(getViewModel().searchQuery, false);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(ListActivity.this, UserDetailsActivity.class);
                intent.putExtra(UserDetailsActivity.USER_ID, query);
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                getViewModel().searchQuery = query;
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorites:
                Intent intent = new Intent(this, FavoritesActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }
}

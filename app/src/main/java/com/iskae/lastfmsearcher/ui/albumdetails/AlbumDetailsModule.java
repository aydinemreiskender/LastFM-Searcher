package com.iskae.lastfmsearcher.ui.albumdetails;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AlbumDetailsModule {

    @ContributesAndroidInjector
    abstract AlbumDetailsFragment contributesAlbumDetailsFragment();

}
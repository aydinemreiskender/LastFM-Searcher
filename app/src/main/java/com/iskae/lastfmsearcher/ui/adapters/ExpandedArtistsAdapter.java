package com.iskae.lastfmsearcher.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.data.model.Artist;
import com.iskae.lastfmsearcher.databinding.ArtistsListItemExpandedBinding;
import com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity;
import com.iskae.lastfmsearcher.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;

public class ExpandedArtistsAdapter extends RecyclerView.Adapter<ExpandedArtistsAdapter.ViewHolder> {
    private Context context;
    private List<Artist> artists;

    public ExpandedArtistsAdapter(Context context, List<Artist> artists) {
        this.context = context;
        this.artists = artists;
    }

    @NonNull
    @Override
    public ExpandedArtistsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ArtistsListItemExpandedBinding binding = ArtistsListItemExpandedBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ExpandedArtistsAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpandedArtistsAdapter.ViewHolder holder, int position) {
        Artist artist = artists.get(position);
        if (artist != null) {
            holder.binding.setArtist(artist);
            holder.binding.tvArtistRank.setText(context.getString(R.string.rank_number, String.valueOf(position + 1)));
            String imageUrl = ImageUtils.getSuitableImageUrl(artist.getImages());
            if (imageUrl != null && imageUrl.length() > 0) {
                holder.binding.imageView.setVisibility(View.VISIBLE);
                holder.binding.tvArtistName.setVisibility(View.VISIBLE);
                Picasso.get().load(ImageUtils.getSuitableImageUrl(artist.getImages())).into(holder.binding.imageView);
            } else {
                holder.binding.imageView.setVisibility(View.GONE);
                holder.binding.tvArtistName.setVisibility(View.GONE);
            }

            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, ArtistDetailsActivity.class);
                intent.putExtra(ARTIST_NAME, artist.getArtistName());
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        if (artists == null) return 0;
        return artists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ArtistsListItemExpandedBinding binding;

        public ViewHolder(ArtistsListItemExpandedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}


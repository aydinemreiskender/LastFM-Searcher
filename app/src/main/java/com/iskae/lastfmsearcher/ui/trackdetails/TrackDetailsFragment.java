package com.iskae.lastfmsearcher.ui.trackdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentTrackDetailsBinding;
import com.iskae.lastfmsearcher.ui.adapters.TracksAdapter;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

import java.util.ArrayList;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;
import static com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsActivity.TRACK_NAME;
import static com.iskae.lastfmsearcher.util.Helpers.buildTagsText;

public class TrackDetailsFragment extends BaseFragment<TrackDetailsViewModel, FragmentTrackDetailsBinding> {

    private FragmentTrackDetailsBinding binding;
    private TracksAdapter similarExpandedTracksAdapter;

    public static TrackDetailsFragment newInstance(String artistName, String trackName) {
        TrackDetailsFragment fragment = new TrackDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARTIST_NAME, artistName);
        args.putString(TRACK_NAME, trackName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        initSimilarTracksAdapter();
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                String artistName = getArguments().getString(ARTIST_NAME);
                String trackName = getArguments().getString(TRACK_NAME);
                if (trackName != null) {
                    getViewModel().loadTrackInfo(artistName, trackName);
                    getViewModel().loadSimilarTracks(artistName, trackName);
                }
            }
        }
        getViewModel().trackResource.observe(this, trackResource -> {
            if (trackResource.getData() != null) {
                setTitle(trackResource.getData().getName());
                binding.setTrack(trackResource.getData());
                binding.tvTags.setText(Html.fromHtml(buildTagsText(trackResource.getData().getTagsResponse().getTags())));
                binding.tvTags.setMovementMethod(LinkMovementMethod.getInstance());
                binding.tvTrackContent.setMovementMethod(LinkMovementMethod.getInstance());
            }
        });
        getViewModel().similarTracksResource.observe(this, similarTracksResource -> {
            binding.setSimilarTracksResource(similarTracksResource);
            if (similarTracksResource.getData() != null && similarTracksResource.getData().size() > 0) {
                binding.similarTracksContainer.setVisibility(View.VISIBLE);
                similarExpandedTracksAdapter.setTracks(similarTracksResource.getData());
            } else {
                binding.similarTracksContainer.setVisibility(View.GONE);
            }
        });

        getTracker().setScreenName(TrackDetailsFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void initSimilarTracksAdapter() {
        similarExpandedTracksAdapter = new TracksAdapter(getContext(), new ArrayList<>());
        RecyclerView.LayoutManager similarTracksLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvSimilarTracks.setLayoutManager(similarTracksLayoutManager);
        binding.rvSimilarTracks.setItemAnimator(new DefaultItemAnimator());
        binding.rvSimilarTracks.setAdapter(similarExpandedTracksAdapter);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_track_details;
    }

    @Override
    public TrackDetailsViewModel getViewModel() {
        return ViewModelProviders.of(this, viewModelFactory).get(TrackDetailsViewModel.class);
    }
}

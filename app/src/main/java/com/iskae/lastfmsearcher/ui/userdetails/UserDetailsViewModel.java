package com.iskae.lastfmsearcher.ui.userdetails;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.iskae.lastfmsearcher.core.base.BaseViewModel;
import com.iskae.lastfmsearcher.core.data.LastFmRepository;
import com.iskae.lastfmsearcher.core.data.model.Artist;
import com.iskae.lastfmsearcher.core.data.model.Resource;
import com.iskae.lastfmsearcher.core.data.model.Status;
import com.iskae.lastfmsearcher.core.data.model.Track;
import com.iskae.lastfmsearcher.core.data.model.User;
import com.iskae.lastfmsearcher.util.Utils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Emre on 18.03.18
 */

public class UserDetailsViewModel extends BaseViewModel {

    final MutableLiveData<Resource<List<Artist>>> userTopArtists = new MutableLiveData<>();
    final MutableLiveData<Resource<List<Track>>> userTopTracks = new MutableLiveData<>();
    final MutableLiveData<Resource<User>> user = new MutableLiveData<>();
    final MutableLiveData<Boolean> userExistsInDb = new MutableLiveData<>();
    final MutableLiveData<Integer> currentTab = new MutableLiveData<>();

    private String userId;

    @Inject
    public UserDetailsViewModel(Application application, LastFmRepository repository) {
        super(application, repository);
    }

    private void loadUserTopTracks() {
        getCompositeDisposable().add(getLastFmRepository().getUserTopTracks(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> userTopTracks.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(topTracksResponse -> {
                    if (topTracksResponse != null) {
                        if (topTracksResponse.body() != null && topTracksResponse.body().getUserTopTracks().getTracks() != null) {
                            userTopTracks.postValue(new Resource<>(topTracksResponse.body().getUserTopTracks().getTracks(), Status.SUCCESS, null));
                        } else {
                            userTopTracks.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(topTracksResponse.errorBody())));
                        }
                    }
                }, throwable -> userTopTracks.postValue(new Resource<>(null, Status.ERROR, throwable.getMessage()))));
    }

    private void loadUserTopArtists() {
        getCompositeDisposable().add(getLastFmRepository().getUserTopArtists(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> userTopArtists.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(topArtistsResponse -> {
                    if (topArtistsResponse != null) {
                        if (topArtistsResponse.body() != null && topArtistsResponse.body().getTopArtists().getArtists() != null) {
                            userTopArtists.postValue(new Resource<>(topArtistsResponse.body().getTopArtists().getArtists(), Status.SUCCESS, null));
                        } else {
                            userTopArtists.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(topArtistsResponse.errorBody())));
                        }
                    }
                }, throwable -> userTopArtists.postValue(new Resource<>(null, Status.ERROR, throwable.getMessage()))));
    }

    private void loadUserInfo() {
        getCompositeDisposable().add(getLastFmRepository().getUserInfo(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> user.postValue(new Resource<>(null, Status.LOADING, null)))
                .subscribe(userResponse -> {
                    if (userResponse != null) {
                        if (userResponse.body() != null) {
                            user.postValue(new Resource<>(userResponse.body().getUser(), Status.SUCCESS, null));
                            loadUserFromDb(userResponse.body().getUser());
                            loadUserTopArtists();
                            loadUserTopTracks();
                        } else {
                            user.postValue(new Resource<>(null, Status.ERROR, Utils.getErrorMessage(userResponse.errorBody())));
                        }
                    }
                }, throwable -> user.postValue(new Resource<>(null, Status.ERROR, throwable.getMessage()))));
    }

    private void loadUserFromDb(User user) {
        getCompositeDisposable().add(getLastFmRepository().getUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> userExistsInDb.postValue(false))
                .doOnComplete(() -> userExistsInDb.postValue(false))
                .subscribe(userResponse -> userExistsInDb.postValue(true)));
    }

    public void setUserId(String userId) {
        this.userId = userId;
        loadUserInfo();
    }

    public void insertUserAsFavorite() {
        getCompositeDisposable().add(Completable.fromAction(() -> getLastFmRepository().insertUserAsFavorite(user.getValue().getData()))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> loadUserFromDb(user.getValue().getData())));
    }

    public void removeUserFromFavorites() {
        getCompositeDisposable().add(Completable.fromAction(() -> getLastFmRepository().removeUserFromFavorites(user.getValue().getData()))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> loadUserFromDb(user.getValue().getData())));
    }
}

package com.iskae.lastfmsearcher.ui.albumdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentAlbumDetailsBinding;
import com.iskae.lastfmsearcher.ui.adapters.TracksAdapter;

import java.util.ArrayList;

import static android.view.View.VISIBLE;
import static com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity.ALBUM_NAME;
import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;
import static com.iskae.lastfmsearcher.util.Helpers.buildTagsText;

public class AlbumDetailsFragment extends BaseFragment<AlbumDetailsViewModel, FragmentAlbumDetailsBinding> {

    private FragmentAlbumDetailsBinding binding;
    private TracksAdapter tracksAdapter;

    public static AlbumDetailsFragment newInstance(String artistName, String albumName) {
        AlbumDetailsFragment fragment = new AlbumDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARTIST_NAME, artistName);
        args.putString(ALBUM_NAME, albumName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        initTracksAdapter();
        if (savedInstanceState == null) {
            if (getArguments() != null) {
                String artistName = getArguments().getString(ARTIST_NAME);
                String albumName = getArguments().getString(ALBUM_NAME);
                if (albumName != null) {
                    getViewModel().loadAlbumDetails(artistName, albumName);
                }
            }
        }
        getViewModel().albumResource.observe(this, albumResource -> {
            if (albumResource.getData() != null) {
                setTitle(albumResource.getData().getName());
                binding.setAlbum(albumResource.getData());
                binding.tvTags.setText(Html.fromHtml(buildTagsText(albumResource.getData().getTagsResponse().getTags())));
                binding.tvTags.setMovementMethod(LinkMovementMethod.getInstance());
                binding.tvAlbumContent.setMovementMethod(LinkMovementMethod.getInstance());
                if (albumResource.getData().getTracksResponse().getTracks().size() > 0) {
                    binding.albumTracksContainer.setVisibility(VISIBLE);
                    tracksAdapter.setTracks(albumResource.getData().getTracksResponse().getTracks());
                } else {
                    binding.albumTracksContainer.setVisibility(View.GONE);
                }
            }
        });

        getTracker().setScreenName(AlbumDetailsFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void initTracksAdapter() {
        tracksAdapter = new TracksAdapter(getContext(), new ArrayList<>());
        RecyclerView.LayoutManager tracksLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvAlbumTracks.setLayoutManager(tracksLayoutManager);
        binding.rvAlbumTracks.setItemAnimator(new DefaultItemAnimator());
        binding.rvAlbumTracks.setAdapter(tracksAdapter);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_album_details;
    }

    @Override
    public AlbumDetailsViewModel getViewModel() {
        return ViewModelProviders.of(this, viewModelFactory).get(AlbumDetailsViewModel.class);
    }
}

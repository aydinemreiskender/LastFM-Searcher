package com.iskae.lastfmsearcher.ui.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.data.model.Track;
import com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsActivity;
import com.iskae.lastfmsearcher.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;
import static com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsActivity.TRACK_NAME;

public class WidgetTracksProvider implements RemoteViewsService.RemoteViewsFactory {

    private List<Track> tracksList;
    private Context context;
    private int appWidgetId;

    @Inject
    WidgetUtils widgetUtils;

    WidgetTracksProvider(Context context, Intent intent) {
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        tracksList = WidgetFetchDataService.trackList;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return tracksList != null ? tracksList.size() : 0;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        final RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.widget_tracks_list_item);
        Track track = tracksList.get(position);
        Bitmap bitmap = null;
        try {
            bitmap = Picasso.get().load(ImageUtils.getSuitableImageUrl(track.getImages())).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bitmap != null) {
            remoteView.setImageViewBitmap(R.id.imageView, bitmap);
        } else {
            remoteView.setImageViewResource(R.id.imageView, R.drawable.ic_logo);
        }
        remoteView.setTextViewText(R.id.tvRankView, String.valueOf(position + 1));
        remoteView.setTextViewText(R.id.tvSongName, track.getName());
        remoteView.setTextViewText(R.id.tvArtistName, track.getArtist().getArtistName());
        remoteView.setTextViewText(R.id.tvPlayCount, String.valueOf(track.getPlayCount()));

        Intent intent = new Intent();
        intent.putExtra(ARTIST_NAME, track.getArtist().getArtistName());
        intent.putExtra(TRACK_NAME, track.getName());
        Bundle extras = new Bundle();
        extras.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.putExtras(extras);

        remoteView.setOnClickFillInIntent(R.id.trackContainer, intent);
        return remoteView;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

}

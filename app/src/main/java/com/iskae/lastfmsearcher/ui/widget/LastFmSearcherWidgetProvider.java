package com.iskae.lastfmsearcher.ui.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity;
import com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsActivity;

public class LastFmSearcherWidgetProvider extends AppWidgetProvider {
    public static final String DATA_FETCHED = "com.iskae.lastfmsearcher.DATA_FETCHED";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            Intent serviceIntent = new Intent(context, WidgetFetchDataService.class);
            serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    appWidgetId);
            context.startService(serviceIntent);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }


    private static RemoteViews updateWidgetListView(Context context, int appWidgetId) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.app_widget_layout);

        Intent svcIntent = new Intent(context, WidgetService.class);
        svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));
        remoteViews.setRemoteAdapter(R.id.listViewWidget,
                svcIntent);
        remoteViews.setEmptyView(R.id.listViewWidget, R.id.empty_view);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String itemType = preferences.getString(String.valueOf(appWidgetId), null);
        if (itemType != null) {
            if (itemType.equals(WidgetUtils.PREF_TRACKS)) {
                Intent clickIntent = new Intent(context, TrackDetailsActivity.class);
                PendingIntent clickPendingIntent = PendingIntent
                        .getActivity(context, 0,
                                clickIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setPendingIntentTemplate(R.id.listViewWidget, clickPendingIntent);
            } else {
                Intent clickIntent = new Intent(context, ArtistDetailsActivity.class);
                PendingIntent clickPendingIntent = PendingIntent
                        .getActivity(context, 0,
                                clickIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setPendingIntentTemplate(R.id.listViewWidget, clickPendingIntent);
            }
        }
        return remoteViews;
    }

    public static void updateWidget(Context context, Intent intent) {
        int appWidgetId = intent.getIntExtra(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        AppWidgetManager appWidgetManager = AppWidgetManager
                .getInstance(context);
        RemoteViews remoteViews = updateWidgetListView(context, appWidgetId);
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }
}

package com.iskae.lastfmsearcher.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.data.model.Artist;
import com.iskae.lastfmsearcher.databinding.ArtistsListItemBinding;
import com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity;

import java.util.List;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;

/**
 * @author Emre on 18.03.18
 */

public class ArtistsAdapter extends RecyclerView.Adapter<ArtistsAdapter.ViewHolder> {
    private Context context;
    private List<Artist> artists;

    public ArtistsAdapter(Context context, List<Artist> artists) {
        this.context = context;
        this.artists = artists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ArtistsListItemBinding binding = ArtistsListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Artist artist = artists.get(position);
        if (artist != null) {
            holder.binding.setArtist(artist);
            holder.binding.tvArtistRank.setText(context.getString(R.string.rank_number, String.valueOf(position + 1)));
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, ArtistDetailsActivity.class);
                intent.putExtra(ARTIST_NAME, artist.getArtistName());
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        if (artists == null) return 0;
        return artists.size();
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ArtistsListItemBinding binding;

        public ViewHolder(ArtistsListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}

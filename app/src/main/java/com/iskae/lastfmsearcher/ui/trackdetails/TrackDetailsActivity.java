package com.iskae.lastfmsearcher.ui.trackdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.android.databinding.library.baseAdapters.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseActivity;
import com.iskae.lastfmsearcher.databinding.ActivityMasterBinding;

import static com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity.ARTIST_NAME;

public class TrackDetailsActivity extends BaseActivity<TrackDetailsViewModel, ActivityMasterBinding> {

    public static final String TRACK_NAME = "TRACK_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMasterBinding binding = getViewDataBinding();
        String artistName = getIntent().getStringExtra(ARTIST_NAME);
        String albumName = getIntent().getStringExtra(TRACK_NAME);
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if (albumName != null) {
            addFragmentToActivity(getSupportFragmentManager(), TrackDetailsFragment.newInstance(artistName, albumName),
                    R.id.contentContainer, TrackDetailsFragment.class.getSimpleName());
        }
    }


    @Override
    public TrackDetailsViewModel getViewModel() {
        return ViewModelProviders.of(this, viewModelFactory).get(TrackDetailsViewModel.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_master;
    }

}

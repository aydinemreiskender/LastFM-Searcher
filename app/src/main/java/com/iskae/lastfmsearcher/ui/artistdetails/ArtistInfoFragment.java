package com.iskae.lastfmsearcher.ui.artistdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.BR;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.core.data.model.Artist;
import com.iskae.lastfmsearcher.databinding.FragmentArtistInfoBinding;
import com.iskae.lastfmsearcher.ui.adapters.ArtistsAdapter;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

import java.util.ArrayList;

import static com.iskae.lastfmsearcher.util.Helpers.buildTagsText;

public class ArtistInfoFragment extends BaseFragment<ArtistViewModel, FragmentArtistInfoBinding> {

    private FragmentArtistInfoBinding binding;

    private ArtistsAdapter similarArtistsAdapter;

    public static ArtistInfoFragment newInstance() {
        return new ArtistInfoFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        similarArtistsAdapter = new ArtistsAdapter(getContext(), new ArrayList<Artist>());
        RecyclerView.LayoutManager similarArtistsLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvSimilarArtists.setLayoutManager(similarArtistsLayoutManager);
        binding.rvSimilarArtists.setItemAnimator(new DefaultItemAnimator());
        binding.rvSimilarArtists.setAdapter(similarArtistsAdapter);

        getViewModel().artistResource.observe(this, artistResource -> {
            if (artistResource.getData() != null) {
                setTitle(artistResource.getData().getArtistName());
                binding.setArtist(artistResource.getData());
                binding.tvTags.setText(Html.fromHtml(buildTagsText(artistResource.getData().getTagsResponse().getTags())));
                binding.tvTags.setMovementMethod(LinkMovementMethod.getInstance());
                binding.tvArtistContent.setMovementMethod(LinkMovementMethod.getInstance());
            }
        });
        getViewModel().similarArtistsResource.observe(this, similarArtistsResource -> {
            binding.setSimilarArtistsResource(similarArtistsResource);
            similarArtistsAdapter.setArtists(similarArtistsResource.getData());
        });

        getTracker().setScreenName(ArtistInfoFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_artist_info;
    }

    @Override
    public ArtistViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(ArtistViewModel.class);
    }

}

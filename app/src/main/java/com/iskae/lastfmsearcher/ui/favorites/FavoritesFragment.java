package com.iskae.lastfmsearcher.ui.favorites;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.google.android.gms.analytics.HitBuilders;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.base.BaseFragment;
import com.iskae.lastfmsearcher.databinding.FragmentFavoritesBinding;
import com.iskae.lastfmsearcher.ui.adapters.FavoriteUsersAdapter;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;

import java.util.ArrayList;

public class FavoritesFragment extends BaseFragment<FavoritesViewModel, FragmentFavoritesBinding> {
    private FragmentFavoritesBinding binding;
    private FavoriteUsersAdapter favoriteUsersAdapter;

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = getViewDataBinding();
        initFavoriteUsersAdapter();
        if (savedInstanceState == null) {
            getViewModel().loadFavoriteUsers();
        }
        getViewModel().favoriteUsers.observe(this, favoriteUsersResource -> {
            binding.setFavoriteUsersResource(favoriteUsersResource);
            favoriteUsersAdapter.setUsers(favoriteUsersResource.getData());
        });
        getTracker().setScreenName(FavoritesFragment.class.getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void initFavoriteUsersAdapter() {
        favoriteUsersAdapter = new FavoriteUsersAdapter(getContext(), new ArrayList<>());
        RecyclerView.LayoutManager similarTracksLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                LinearLayoutManager.VERTICAL);
        binding.rvFavoriteUsers.setLayoutManager(similarTracksLayoutManager);
        binding.rvFavoriteUsers.setItemAnimator(new DefaultItemAnimator());
        binding.rvFavoriteUsers.setAdapter(favoriteUsersAdapter);
        binding.rvFavoriteUsers.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_favorites;
    }

    @Override
    public FavoritesViewModel getViewModel() {
        return ViewModelProviders.of(getActivity(), viewModelFactory).get(FavoritesViewModel.class);
    }

}

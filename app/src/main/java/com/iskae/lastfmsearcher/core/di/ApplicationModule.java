package com.iskae.lastfmsearcher.core.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.iskae.lastfmsearcher.core.data.LastFmRepository;
import com.iskae.lastfmsearcher.core.data.source.local.FavoriteUsersDao;
import com.iskae.lastfmsearcher.core.data.source.local.FavoriteUsersDatabase;
import com.iskae.lastfmsearcher.core.data.source.local.LocalLastFmRepository;
import com.iskae.lastfmsearcher.core.data.source.remote.RemoteLastFmRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Emre on 17.03.18
 */

@Module(includes = ViewModelModule.class)
public class ApplicationModule {

    @Singleton
    @Provides
    public Context provideContext(Application application) {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    public SharedPreferences providesSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    FavoriteUsersDao provideFavoriteUsersDao(FavoriteUsersDatabase database) {
        return database.userDao();
    }

    @Provides
    @Singleton
    FavoriteUsersDatabase provideFavoriteUsersDatabase(Application application) {
        return Room.databaseBuilder(application,
                FavoriteUsersDatabase.class, "favoriteusers.db").build();
    }

    @Singleton
    @Provides
    public LastFmRepository providesLastFmRepository(RemoteLastFmRepository remoteLastFmRepository,
                                                     LocalLastFmRepository localLastFmRepository) {
        return new LastFmRepository(remoteLastFmRepository, localLastFmRepository);
    }
}

package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class StatsResponse {
    @SerializedName("listeners")
    private long listeners;
    @SerializedName("playcount")
    private long playCount;

    public String getListeners() {
        return String.valueOf(listeners);
    }

    public String getPlays() {
        return String.valueOf(playCount);
    }
}

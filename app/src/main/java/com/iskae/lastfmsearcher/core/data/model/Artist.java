package com.iskae.lastfmsearcher.core.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.response.StatsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TagsResponse;
import com.iskae.lastfmsearcher.util.ImageUtils;

import java.util.List;

import lombok.Data;

/**
 * @author Emre on 17.03.18
 */

@Data
public class Artist {
    @SerializedName("name")
    @Expose
    private String artistName;
    @SerializedName("stats")
    @Expose
    private StatsResponse stats;
    @SerializedName("mbid")
    @Expose
    private String mbid;
    @SerializedName("image")
    @Expose
    private List<LastFmImage> images;
    @SerializedName("tags")
    private TagsResponse tagsResponse;
    @SerializedName("bio")
    @Expose
    private Bio bio;

    public String getSuitableImageUrl() {
        return ImageUtils.getSuitableImageUrl(images);
    }

}

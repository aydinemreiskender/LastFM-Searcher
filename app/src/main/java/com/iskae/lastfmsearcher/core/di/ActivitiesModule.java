package com.iskae.lastfmsearcher.core.di;

import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsActivity;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsModule;
import com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsActivity;
import com.iskae.lastfmsearcher.ui.artistdetails.ArtistDetailsModule;
import com.iskae.lastfmsearcher.ui.favorites.FavoritesActivity;
import com.iskae.lastfmsearcher.ui.favorites.FavoritesModule;
import com.iskae.lastfmsearcher.ui.list.ListActivity;
import com.iskae.lastfmsearcher.ui.list.ListModule;
import com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsActivity;
import com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsModule;
import com.iskae.lastfmsearcher.ui.userdetails.UserDetailsActivity;
import com.iskae.lastfmsearcher.ui.userdetails.UserDetailsModule;
import com.iskae.lastfmsearcher.ui.widget.AppWidgetConfigureActivity;
import com.iskae.lastfmsearcher.ui.widget.WidgetFetchDataService;
import com.iskae.lastfmsearcher.ui.widget.WidgetService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author Emre on 17.03.18
 */

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = {ListModule.class})
    abstract ListActivity contributesListActivity();

    @ContributesAndroidInjector(modules = {UserDetailsModule.class})
    abstract UserDetailsActivity contributesUserDetailsActivity();

    @ContributesAndroidInjector(modules = {ArtistDetailsModule.class})
    abstract ArtistDetailsActivity contributesArtistDetailsActivity();

    @ContributesAndroidInjector(modules = {AlbumDetailsModule.class})
    abstract AlbumDetailsActivity contributesAlbumDetailsActivity();

    @ContributesAndroidInjector(modules = {TrackDetailsModule.class})
    abstract TrackDetailsActivity contributesTrackDetailsActivity();

    @ContributesAndroidInjector(modules = {FavoritesModule.class})
    abstract FavoritesActivity contributesFavoritesActivity();

    @ContributesAndroidInjector
    abstract AppWidgetConfigureActivity contributesAppWidgetConfigureActivtiy();

    @ContributesAndroidInjector
    abstract WidgetService contributesWidgetService();

    @ContributesAndroidInjector
    abstract WidgetFetchDataService contributesWidgetFetchDataService();
}

package com.iskae.lastfmsearcher.core.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.iskae.lastfmsearcher.core.base.ViewModelFactory;
import com.iskae.lastfmsearcher.ui.albumdetails.AlbumDetailsViewModel;
import com.iskae.lastfmsearcher.ui.artistdetails.ArtistViewModel;
import com.iskae.lastfmsearcher.ui.favorites.FavoritesViewModel;
import com.iskae.lastfmsearcher.ui.list.ListViewModel;
import com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsViewModel;
import com.iskae.lastfmsearcher.ui.userdetails.UserDetailsViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AlbumDetailsViewModel.class)
    abstract ViewModel bindAlbumDetailsViewModel(AlbumDetailsViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ArtistViewModel.class)
    abstract ViewModel bindArtistViewModel(ArtistViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FavoritesViewModel.class)
    abstract ViewModel bindsFavoritesViewModel(FavoritesViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel.class)
    abstract ViewModel bindsListViewModel(ListViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TrackDetailsViewModel.class)
    abstract ViewModel bindsTrackDetailsViewModel(TrackDetailsViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailsViewModel.class)
    abstract ViewModel bindsUserDetailsViewModel(UserDetailsViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
package com.iskae.lastfmsearcher.core.data.model;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}

package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.Track;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Emre on 18.03.18
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class TracksResponse {
    @SerializedName("track")
    private List<Track> tracks;
}

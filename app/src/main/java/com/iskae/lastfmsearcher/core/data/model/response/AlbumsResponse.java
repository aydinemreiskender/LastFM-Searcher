package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.Album;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AlbumsResponse {
    @SerializedName("album")
    private List<Album> albums;
}
package com.iskae.lastfmsearcher.core.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.util.ImageUtils;

import java.util.List;

import lombok.Data;

/**
 * @author Emre on 17.03.18
 */

@Entity
@Data
public class User {
    @PrimaryKey
    @NonNull
    @SerializedName("name")
    private String name = "";
    @SerializedName("realname")
    private String realName;
    @Ignore
    @SerializedName("image")
    private List<LastFmImage> imageList;
    @SerializedName("url")
    private String url;
    @SerializedName("country")
    private String country;
    @SerializedName("gender")
    private String gender;
    @SerializedName("age")
    private int age;
    @SerializedName("playcount")
    private int playCount;
    @SerializedName("playlists")
    private int playlistCount;
    @SerializedName("subscriber")
    private int subscriberCount;
    private long registeredDate;

    public String getSuitableImageUrl() {
        return ImageUtils.getSuitableImageUrl(imageList);
    }
}

package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ArtistTopTracksResponse {
    @SerializedName("toptracks")
    private TracksResponse tracksResponse;
}
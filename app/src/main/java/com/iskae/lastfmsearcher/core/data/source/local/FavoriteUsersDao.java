package com.iskae.lastfmsearcher.core.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.iskae.lastfmsearcher.core.data.model.User;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface FavoriteUsersDao {
    @Query("SELECT * FROM User")
    Flowable<List<User>> getAllFavoriteUsers();

    @Query("SELECT * FROM User WHERE name = :name")
    Maybe<User> getUserByName(String name);

    @Query("DELETE FROM User")
    void deleteUsers();

    @Query("DELETE FROM User WHERE name = :name")
    void deleteUserByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);

}
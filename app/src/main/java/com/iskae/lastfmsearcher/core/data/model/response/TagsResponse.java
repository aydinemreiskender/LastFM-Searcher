package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.Tag;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TagsResponse {
    @SerializedName("tag")
    private List<Tag> tags;
}

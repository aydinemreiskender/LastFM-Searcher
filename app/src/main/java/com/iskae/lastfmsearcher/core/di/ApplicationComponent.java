package com.iskae.lastfmsearcher.core.di;

import android.app.Application;

import com.iskae.lastfmsearcher.core.base.LastFmSearcherApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * @author Emre on 17.03.18
 */

@Singleton
@Component(modules = {AndroidInjectionModule.class, ApplicationModule.class, NetworkModule.class, ActivitiesModule.class})
public interface ApplicationComponent {
    void inject(LastFmSearcherApplication app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        ApplicationComponent build();
    }
}

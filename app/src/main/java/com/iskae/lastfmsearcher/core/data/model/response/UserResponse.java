package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Emre on 18.03.18
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class UserResponse {
    @SerializedName("user")
    private User user;
}

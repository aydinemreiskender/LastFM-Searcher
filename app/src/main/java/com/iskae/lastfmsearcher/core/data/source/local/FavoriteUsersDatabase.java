package com.iskae.lastfmsearcher.core.data.source.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.iskae.lastfmsearcher.core.data.model.User;

@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class FavoriteUsersDatabase extends RoomDatabase {

    private static FavoriteUsersDatabase INSTANCE;

    public static FavoriteUsersDatabase getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.inMemoryDatabaseBuilder(context.getApplicationContext(), FavoriteUsersDatabase.class)
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    public abstract FavoriteUsersDao userDao();
}

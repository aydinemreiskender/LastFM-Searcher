package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Emre on 20.03.18
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class TopArtistsResponse  {
    @SerializedName("artists")
    private ArtistsResponse artistsResponse;
}

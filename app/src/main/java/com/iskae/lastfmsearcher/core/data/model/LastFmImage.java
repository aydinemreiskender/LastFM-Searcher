package com.iskae.lastfmsearcher.core.data.model;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * @author Emre on 17.03.18
 */

@Entity
@Data
public class LastFmImage {
    @SerializedName("#text")
    private String url;
    @SerializedName("size")
    private String size;
}

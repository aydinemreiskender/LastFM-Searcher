package com.iskae.lastfmsearcher.core.base;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import com.iskae.lastfmsearcher.core.data.LastFmRepository;

import io.reactivex.disposables.CompositeDisposable;

/**
 * @author Emre on 17.03.18
 */

public abstract class BaseViewModel extends AndroidViewModel {

    private final CompositeDisposable disposables = new CompositeDisposable();

    private LastFmRepository lastFmRepository;

    public BaseViewModel(Application application, LastFmRepository repository) {
        super(application);
        this.lastFmRepository = repository;
    }

    public CompositeDisposable getCompositeDisposable() {
        return disposables;
    }

    public LastFmRepository getLastFmRepository() {
        return lastFmRepository;
    }

    @Override
    protected void onCleared() {
        disposables.dispose();
        super.onCleared();
    }
}
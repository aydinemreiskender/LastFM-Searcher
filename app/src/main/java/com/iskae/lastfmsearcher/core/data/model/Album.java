package com.iskae.lastfmsearcher.core.data.model;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.response.TagsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.WikiResponse;
import com.iskae.lastfmsearcher.util.ImageUtils;

import java.util.List;

import lombok.Data;

@Data
public class Album {
    @SerializedName("name")
    private String name;
    @SerializedName("mbid")
    private String mBid;
    @SerializedName("listeners")
    private long listeners;
    @SerializedName("playcount")
    private long playCount;
    @SerializedName("url")
    private String url;
    @SerializedName("image")
    private List<LastFmImage> images;
    @SerializedName("tags")
    private TagsResponse tagsResponse;
    @SerializedName("tracks")
    private TracksResponse tracksResponse;
    @SerializedName("wiki")
    private WikiResponse wikiResponse;

    public String getSuitableImageUrl() {
        return ImageUtils.getSuitableImageUrl(images);
    }

}

package com.iskae.lastfmsearcher.core.data;


import com.iskae.lastfmsearcher.core.data.model.User;
import com.iskae.lastfmsearcher.core.data.model.response.AlbumResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistTopAlbumsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistTopTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.GeoTopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.SimilarArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.SimilarTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TopTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TrackResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserTopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserTopTracksResponse;
import com.iskae.lastfmsearcher.core.data.source.local.LocalLastFmRepository;
import com.iskae.lastfmsearcher.core.data.source.remote.RemoteLastFmRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import retrofit2.Response;

/**
 * @author Emre on 17.03.18
 */

public class LastFmRepository {

    private RemoteLastFmRepository remoteLastFmRepository;
    private LocalLastFmRepository localLastFmRepository;

    @Inject
    public LastFmRepository(RemoteLastFmRepository remoteLastFmRepository, LocalLastFmRepository localLastFmRepository) {
        this.remoteLastFmRepository = remoteLastFmRepository;
        this.localLastFmRepository = localLastFmRepository;
    }

    public Single<Response<TopArtistsResponse>> getWorldTopArtistsList() {
        return remoteLastFmRepository.getWorldTopArtists();
    }

    public Single<Response<TopTracksResponse>> getWorldTopTracksList() {
        return remoteLastFmRepository.getWorldTopTracks();
    }

    public Single<Response<GeoTopArtistsResponse>> getGeoTopArtistsList(String location) {
        return remoteLastFmRepository.getGeoTopArtists(location);
    }

    public Single<Response<TopTracksResponse>> getGeoTopTracksList(String location) {
        return remoteLastFmRepository.getGeoTopTracks(location);
    }

    public Single<Response<UserResponse>> getUserInfo(String userId) {
        return remoteLastFmRepository.getUserInfo(userId);
    }

    public Single<Response<UserTopTracksResponse>> getUserTopTracks(String userId) {
        return remoteLastFmRepository.getUserTopTracks(userId);
    }

    public Single<Response<UserTopArtistsResponse>> getUserTopArtists(String userId) {
        return remoteLastFmRepository.getUserTopArtists(userId);
    }

    public Single<Response<ArtistResponse>> getArtistInfo(String artistName) {
        return remoteLastFmRepository.getArtistInfo(artistName);
    }

    public Single<Response<SimilarArtistsResponse>> getSimilarArtists(String artistName) {
        return remoteLastFmRepository.getSimilarArtists(artistName);
    }

    public Single<Response<ArtistTopAlbumsResponse>> getArtistTopAlbums(String artistName) {
        return remoteLastFmRepository.getArtistTopAlbums(artistName);
    }

    public Single<Response<ArtistTopTracksResponse>> getArtistTopTracks(String artistName) {
        return remoteLastFmRepository.getArtistTopTracks(artistName);
    }

    public Single<Response<AlbumResponse>> getAlbumDetails(String artistName, String albumName) {
        return remoteLastFmRepository.getAlbumDetails(artistName, albumName);
    }

    public Single<Response<TrackResponse>> getTrackDetails(String artistName, String trackName) {
        return remoteLastFmRepository.getTrackDetails(artistName, trackName);
    }

    public Single<Response<SimilarTracksResponse>> getSimilarTracks(String artistName, String trackName) {
        return remoteLastFmRepository.getSimilarTracks(artistName, trackName);
    }

    public Maybe<User> getUser(User user) {
        return localLastFmRepository.getUser(user);
    }

    public void insertUserAsFavorite(User user) {
        localLastFmRepository.insertUser(user);
    }

    public void removeUserFromFavorites(User user) {
        localLastFmRepository.deleteUser(user);
    }

    public Flowable<List<User>> getAllFavoriteUsers() {
        return localLastFmRepository.getAllFavoriteUsers();
    }
}

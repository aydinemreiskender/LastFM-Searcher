package com.iskae.lastfmsearcher.core.data.source.remote;


import com.iskae.lastfmsearcher.core.data.model.response.AlbumResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistTopAlbumsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistTopTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.GeoTopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.SimilarArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.SimilarTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TopTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TrackResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserTopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserTopTracksResponse;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Emre on 18.03.18
 */

public interface LastFmService {

    @GET("?method=chart.gettopartists&format=json")
    Single<Response<TopArtistsResponse>> getWorldTopArtists(@Query("api_key") String apiKey);

    @GET("?method=chart.gettoptracks&format=json")
    Single<Response<TopTracksResponse>> getWorldTopTracks(@Query("api_key") String apiKey);

    @GET("?method=geo.gettopartists&format=json")
    Single<Response<GeoTopArtistsResponse>> getGeoTopArtists(@Query("api_key") String apiKey, @Query("country") String countryName);

    @GET("?method=geo.gettoptracks&format=json")
    Single<Response<TopTracksResponse>> getGeoTopTracks(@Query("api_key") String apiKey, @Query("country") String countryName);

    @GET("?method=user.getinfo&format=json")
    Single<Response<UserResponse>> getUserInfo(@Query("user") String user, @Query("api_key") String apiKey);

    @GET("?method=user.gettoptracks&format=json")
    Single<Response<UserTopTracksResponse>> getUserTopTracks(@Query("user") String user, @Query("api_key") String apiKey);

    @GET("?method=user.gettopartists&format=json")
    Single<Response<UserTopArtistsResponse>> getUserTopArtists(@Query("user") String user, @Query("api_key") String apiKey);

    @GET("?method=artist.getinfo&format=json")
    Single<Response<ArtistResponse>> getArtistInfo(@Query("artist") String artistName, @Query("api_key") String apiKey);

    @GET("?method=artist.getsimilar&format=json")
    Single<Response<SimilarArtistsResponse>> getSimilarArtists(@Query("artist") String artistName, @Query("api_key") String apiKey);

    @GET("?method=artist.gettopalbums&format=json")
    Single<Response<ArtistTopAlbumsResponse>> getArtistTopAlbums(@Query("artist") String artistName, @Query("api_key") String apiKey);

    @GET("?method=artist.gettoptracks&format=json")
    Single<Response<ArtistTopTracksResponse>> getArtistTopTracks(@Query("artist") String artistName, @Query("api_key") String apiKey);

    @GET("?method=album.getinfo&format=json")
    Single<Response<AlbumResponse>> getAlbumDetails(@Query("artist") String artistName, @Query("album") String albumName, @Query("api_key") String apiKey);

    @GET("?method=track.getinfo&format=json&autocorrect=1")
    Single<Response<TrackResponse>> getTrackDetails(@Query("artist") String artistName, @Query("track") String trackName, @Query("api_key") String apiKey);

    @GET("?method=track.getsimilar&format=json&autocorrect=1")
    Single<Response<SimilarTracksResponse>> getSimilarTracks(@Query("artist") String artistName, @Query("track") String trackName, @Query("api_key") String apiKey);
}

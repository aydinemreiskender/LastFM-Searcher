package com.iskae.lastfmsearcher.core.data.model;

import android.arch.persistence.room.Entity;
import android.text.Html;
import android.text.Spanned;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Entity
@Data
public class Bio {
    @SerializedName("published")
    private String published;
    @SerializedName("summary")
    private String summary;
    @SerializedName("content")
    private String content;

    public Spanned getFormattedSummary() {
        return Html.fromHtml(summary);
    }

    public Spanned getFormattedContent() {
        return Html.fromHtml(content);
    }
}

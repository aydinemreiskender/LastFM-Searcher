package com.iskae.lastfmsearcher.core.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Tag {
    @SerializedName("name")
    private String tagName;
    @SerializedName("url")
    private String url;

    public String getTag() {
        return "<a href=" + url + ">" + tagName + "</a>";
    }
}

package com.iskae.lastfmsearcher.core.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * @author Emre on 18.03.18
 */

@Data
public class Attribute {
    @SerializedName("rank")
    private int rank;
}

package com.iskae.lastfmsearcher.core.data.source.remote;

import com.iskae.lastfmsearcher.BuildConfig;
import com.iskae.lastfmsearcher.core.data.model.response.AlbumResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistTopAlbumsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.ArtistTopTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.GeoTopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.SimilarArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.SimilarTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TopTracksResponse;
import com.iskae.lastfmsearcher.core.data.model.response.TrackResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserTopArtistsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.UserTopTracksResponse;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;

/**
 * @author Emre on 18.03.18
 */

public class RemoteLastFmRepository {

    private LastFmService service;

    @Inject
    public RemoteLastFmRepository(LastFmService service) {
        this.service = service;
    }

    public Single<Response<TopArtistsResponse>> getWorldTopArtists() {
        return service.getWorldTopArtists(BuildConfig.API_KEY);
    }

    public Single<Response<TopTracksResponse>> getWorldTopTracks() {
        return service.getWorldTopTracks(BuildConfig.API_KEY);
    }

    public Single<Response<GeoTopArtistsResponse>> getGeoTopArtists(String location) {
        return service.getGeoTopArtists(BuildConfig.API_KEY, location);
    }

    public Single<Response<TopTracksResponse>> getGeoTopTracks(String location) {
        return service.getGeoTopTracks(BuildConfig.API_KEY, location);
    }

    public Single<Response<UserResponse>> getUserInfo(String userId) {
        return service.getUserInfo(userId, BuildConfig.API_KEY);
    }

    public Single<Response<UserTopTracksResponse>> getUserTopTracks(String userId) {
        return service.getUserTopTracks(userId, BuildConfig.API_KEY);
    }

    public Single<Response<UserTopArtistsResponse>> getUserTopArtists(String userId) {
        return service.getUserTopArtists(userId, BuildConfig.API_KEY);
    }

    public Single<Response<ArtistResponse>> getArtistInfo(String artistName) {
        return service.getArtistInfo(artistName, BuildConfig.API_KEY);
    }

    public Single<Response<SimilarArtistsResponse>> getSimilarArtists(String artistName) {
        return service.getSimilarArtists(artistName, BuildConfig.API_KEY);
    }

    public Single<Response<ArtistTopAlbumsResponse>> getArtistTopAlbums(String artistName) {
        return service.getArtistTopAlbums(artistName, BuildConfig.API_KEY);
    }

    public Single<Response<ArtistTopTracksResponse>> getArtistTopTracks(String artistName) {
        return service.getArtistTopTracks(artistName, BuildConfig.API_KEY);
    }

    public Single<Response<AlbumResponse>> getAlbumDetails(String artistName, String albumName) {
        return service.getAlbumDetails(artistName, albumName, BuildConfig.API_KEY);
    }

    public Single<Response<TrackResponse>> getTrackDetails(String artistName, String trackName) {
        return service.getTrackDetails(artistName, trackName, BuildConfig.API_KEY);
    }

    public Single<Response<SimilarTracksResponse>> getSimilarTracks(String artistName, String trackName) {
        return service.getSimilarTracks(artistName, trackName, BuildConfig.API_KEY);
    }
}

package com.iskae.lastfmsearcher.core.data.model;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.response.TagsResponse;
import com.iskae.lastfmsearcher.core.data.model.response.WikiResponse;
import com.iskae.lastfmsearcher.util.ImageUtils;

import java.util.List;
import java.util.Locale;

import lombok.Data;

/**
 * @author Emre on 17.03.18
 */

@Data
public class Track {
    @SerializedName("name")
    private String name;
    @SerializedName("duration")
    private int duration;
    @SerializedName("playcount")
    private int playCount;
    @SerializedName("listeners")
    private int listeners;
    @SerializedName("mbid")
    private String mbid;
    @SerializedName("image")
    private List<LastFmImage> images;
    @SerializedName("artist")
    private Artist artist;
    @SerializedName("@attr")
    private Attribute attribute;
    @SerializedName("album")
    private Album album;
    @SerializedName("wiki")
    private WikiResponse wikiResponse;
    @SerializedName("toptags")
    private TagsResponse tagsResponse;


    public String getDurationFormatted() {
        return String.format(Locale.getDefault(), "%02d:%02d", (duration % 3600) / 60, (duration % 60));
    }

    public String getSuitableImageUrl() {
        return ImageUtils.getSuitableImageUrl(images);
    }
}

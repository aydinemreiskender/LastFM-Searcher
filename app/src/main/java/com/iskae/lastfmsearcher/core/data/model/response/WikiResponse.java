package com.iskae.lastfmsearcher.core.data.model.response;

import android.text.Html;
import android.text.Spanned;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class WikiResponse {
    @SerializedName("published")
    private String publishedDate;
    @SerializedName("summary")
    private String summary;
    @SerializedName("content")
    private String content;


    public Spanned getFormattedContent() {
        return Html.fromHtml(content);
    }
}

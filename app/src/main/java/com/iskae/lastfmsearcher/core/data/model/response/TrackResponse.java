package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.Track;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TrackResponse {
    @SerializedName("track")
    private Track track;
}

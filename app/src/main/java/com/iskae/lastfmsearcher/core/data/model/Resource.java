package com.iskae.lastfmsearcher.core.data.model;

import lombok.Data;

@Data
public class Resource<T> {
    private T data;
    private Status status;
    private String errorMessage;

    public Resource(T data, Status status, String errorMessage) {
        this.data = data;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public boolean isLoaded() {
        return data == null && status == Status.ERROR;
    }
}

package com.iskae.lastfmsearcher.core.base;

import android.app.Activity;
import android.app.Application;
import android.app.Service;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.iskae.lastfmsearcher.R;
import com.iskae.lastfmsearcher.core.di.DaggerApplicationComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import timber.log.BuildConfig;
import timber.log.Timber;

/**
 * @author Emre on 17.03.18
 */

public class LastFmSearcherApplication extends Application implements HasActivityInjector, HasServiceInjector {
    @Inject
    DispatchingAndroidInjector<Service> serviceDispatchingAndroidInjector;

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    private static GoogleAnalytics analytics;
    private static Tracker tracker;

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        DaggerApplicationComponent.builder()
                .application(this)
                .build()
                .inject(this);

        analytics = GoogleAnalytics.getInstance(this);

    }

    synchronized public Tracker getDefaultTracker() {
        if (tracker == null) {
            tracker = analytics.newTracker(R.xml.global_tracker);
        }

        return tracker;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceDispatchingAndroidInjector;
    }
}

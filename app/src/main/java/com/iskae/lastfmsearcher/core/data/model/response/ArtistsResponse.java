package com.iskae.lastfmsearcher.core.data.model.response;

import com.google.gson.annotations.SerializedName;
import com.iskae.lastfmsearcher.core.data.model.Artist;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Emre on 20.03.18
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class ArtistsResponse {
    @SerializedName("artist")
    private List<Artist> artists;
}

package com.iskae.lastfmsearcher.core.data.source.local;

import com.iskae.lastfmsearcher.core.data.model.User;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * @author Emre on 18.03.18
 */

public class LocalLastFmRepository {

    private final FavoriteUsersDao favoriteUsersDao;

    @Inject
    public LocalLastFmRepository(FavoriteUsersDao favoriteUsersDao) {
        this.favoriteUsersDao = favoriteUsersDao;
    }

    public Flowable<List<User>> getAllFavoriteUsers() {
        return favoriteUsersDao.getAllFavoriteUsers();
    }

    public void insertUser(User user) {
        favoriteUsersDao.insertUser(user);
    }

    public void deleteUser(User user) {
        favoriteUsersDao.deleteUserByName(user.getName());
    }

    public Maybe<User> getUser(User user) {
        return favoriteUsersDao.getUserByName(user.getName());
    }
}
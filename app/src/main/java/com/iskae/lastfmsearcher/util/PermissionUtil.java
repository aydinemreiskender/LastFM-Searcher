package com.iskae.lastfmsearcher.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * @author Emre on 22.03.18
 */

public class PermissionUtil {

    public static boolean hasAccessLocationPermissions(Context context) {
        return hasAccessCoarseLocationPermission(context) && hasAccessFineLocationPermission(context);
    }

    public static boolean hasAccessFineLocationPermission(Context context) {
        return checkSelfPermissionGranted(context, ACCESS_FINE_LOCATION);
    }

    public static boolean hasAccessCoarseLocationPermission(Context context) {
        return checkSelfPermissionGranted(context, ACCESS_COARSE_LOCATION);
    }

    public static boolean checkSelfPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }
}

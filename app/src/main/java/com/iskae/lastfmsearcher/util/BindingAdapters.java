package com.iskae.lastfmsearcher.util;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.iskae.lastfmsearcher.R;
import com.squareup.picasso.Picasso;

/**
 * Custom BindingAdapter class
 */
public class BindingAdapters {

    /**
     * Toogle view visibility
     *
     * @param view view, which the visibility property be toggled
     * @param show boolean, visibility value
     */
    @BindingAdapter("showView")
    public static void showView(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("imageUrl")
    public static void showView(ImageView view, String url) {
        if (url != null && !url.isEmpty())
            Picasso.get().load(url).placeholder(R.drawable.ic_logo).into(view);
    }

}

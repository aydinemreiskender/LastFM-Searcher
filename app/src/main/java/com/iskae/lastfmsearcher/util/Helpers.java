package com.iskae.lastfmsearcher.util;


import com.iskae.lastfmsearcher.core.data.model.Tag;

import java.util.List;

public class Helpers {

    public static String buildTagsText(List<Tag> tags) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < tags.size(); i++) {
            Tag tag = tags.get(i);
            builder.append(tag.getTag());
            if (i < tags.size() - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }
}

package com.iskae.lastfmsearcher.util;


import com.iskae.lastfmsearcher.core.data.model.LastFmImage;

import java.util.List;

/**
 * @author Emre on 18.03.18
 */

public class ImageUtils {

    public static String getSuitableImageUrl(List<LastFmImage> images) {
        if (images != null && images.size() > 0) {
            for (LastFmImage image : images) {
                if (image.getSize().equalsIgnoreCase("large")) {
                    return image.getUrl();
                }
            }
            //Fallback
            return images.get(0).getUrl();
        }
        return null;
    }
}

<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:bind="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    tools:context=".ui.trackdetails.TrackDetailsActivity">

    <data>

        <import type="android.view.View" />

        <variable
            name="viewModel"
            type="com.iskae.lastfmsearcher.ui.trackdetails.TrackDetailsViewModel" />

        <variable
            name="track"
            type="com.iskae.lastfmsearcher.core.data.model.Track" />

        <import type="com.iskae.lastfmsearcher.core.data.model.Resource" />

        <variable
            name="similarTracksResource"
            type="com.iskae.lastfmsearcher.core.data.model.Resource" />
    </data>

    <ScrollView
        android:id="@+id/svRootView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:orientation="vertical"
            android:padding="@dimen/margin_small">

            <ImageView
                android:id="@+id/ivTrackCover"
                imageUrl="@{track.suitableImageUrl}"
                android:layout_width="160dp"
                android:layout_height="200dp"
                android:layout_gravity="center"
                android:contentDescription="@string/album_image_content_description"
                android:scaleType="centerCrop" />

            <TextView
                android:id="@+id/tvTrackName"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginBottom="@dimen/margin_small"
                android:layout_marginTop="@dimen/margin_small"
                android:gravity="center"
                android:text="@{track.name}"
                android:textAppearance="@style/TextAppearance.AppCompat.Title" />

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginBottom="@dimen/margin_small"
                android:orientation="horizontal">

                <LinearLayout
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:orientation="horizontal">

                    <TextView
                        android:id="@+id/tvListenersLabel"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/margin_small"
                        android:text="@string/artist_info_label_listeners"
                        android:textAppearance="@style/TextAppearance.AppCompat.Subhead" />

                    <TextView
                        android:id="@+id/tvListenersCount"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@{String.valueOf(track.listeners)}" />
                </LinearLayout>

                <LinearLayout
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:orientation="horizontal">

                    <TextView
                        android:id="@+id/tvPlaysLabel"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/margin_small"
                        android:text="@string/artist_info_label_plays"
                        android:textAppearance="@style/TextAppearance.AppCompat.Subhead" />

                    <TextView
                        android:id="@+id/tvPlaysCount"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@{String.valueOf(track.playCount)}" />
                </LinearLayout>

                <LinearLayout
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:orientation="horizontal">

                    <TextView
                        android:id="@+id/tvDurationLabel"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/margin_small"
                        android:text="@string/track_details_duration_label"
                        android:textAppearance="@style/TextAppearance.AppCompat.Subhead" />

                    <TextView
                        android:id="@+id/tvTrackDuration"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@{String.valueOf(track.durationFormatted)}" />
                </LinearLayout>

            </LinearLayout>

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginBottom="@dimen/margin_small"
                android:orientation="horizontal">

                <TextView
                    android:id="@+id/tvTagsLabel"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@string/artist_info_label_tags"
                    android:textAppearance="@style/TextAppearance.AppCompat.Subhead" />

                <TextView
                    android:id="@+id/tvTags"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content" />
            </LinearLayout>


            <TextView
                android:id="@+id/tvTrackContent"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginBottom="@dimen/margin_small"
                android:text="@{track.wikiResponse.formattedContent}" />

            <android.support.v7.widget.CardView
                android:id="@+id/similarTracksContainer"
                style="@style/CardViewStyle"
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="@dimen/margin_small"
                    android:orientation="vertical">

                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="@dimen/margin_normal"
                        android:layout_marginTop="@dimen/margin_small"
                        android:text="@string/track_details_similar_tracks_label"
                        android:textAppearance="@style/TextAppearance.AppCompat.Title" />

                    <android.support.v7.widget.RecyclerView
                        android:id="@+id/rvSimilarTracks"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_margin="@dimen/margin_small"
                        android:orientation="horizontal" />

                    <include
                        android:id="@+id/emptySimilarTracksView"
                        layout="@layout/content_empty_view"
                        bind:resource="@{similarTracksResource}" />

                    <include
                        android:id="@+id/similarTracksLoadingView"
                        layout="@layout/content_loading"
                        bind:resource="@{similarTracksResource}" />
                </LinearLayout>
            </android.support.v7.widget.CardView>
        </LinearLayout>
    </ScrollView>
</layout>